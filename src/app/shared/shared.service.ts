import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { HttpHeaders, HttpResponse, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { SocialAuthService } from "angularx-social-login";
@Injectable({
  providedIn: 'root'
})
export class SharedService {
  httpClient: any;
  // serverUrl = "http://148.72.212.180/CEDDAACAPI/api/";
  serverUrl = "https://api.ceddaac.com:8121/";
  imageBase = 'https://api.ceddaac.com/uploads/'
  isLoggedIn = new BehaviorSubject<boolean>(false);
  activeVendorId = new BehaviorSubject<number>(0);
  activeVendors = new BehaviorSubject<Array<number>>(new Array<number>());
  isLoginOpen = new BehaviorSubject<boolean>(false);
  isSignupOpen = new BehaviorSubject<boolean>(false);
  isEditProfileOpen = new BehaviorSubject<boolean>(false);
  isDirectoryLoginClicked: string;
  isVendorLoginOpen = new BehaviorSubject<boolean>(false);
  isVendorSignupOpen = new BehaviorSubject<boolean>(false);

  isVendorLoggedIn = new BehaviorSubject<boolean>(false);

  VendorPortFolioPopUp = new BehaviorSubject<boolean>(false);
  VendorCategoryData = new BehaviorSubject<Array<number>>(new Array<number>());
  VendorPortFolioPopUpList = new BehaviorSubject<boolean>(false);
  ReachOutPopUp = new BehaviorSubject<boolean>(false);
  ReviewPopUp = new BehaviorSubject<boolean>(false);
  ActiveMenuLink = new BehaviorSubject<string>('');
  PortFolio_PopUp = new BehaviorSubject<boolean>(false);

  Banner_Tempalte = new BehaviorSubject<boolean>(false);
  Vendor_Banner_Tempalte = new BehaviorSubject<boolean>(true);

  Search_Input = new BehaviorSubject<string>('');
  Search_Input_Filter = new BehaviorSubject<Array<number>>(new Array<number>());
  // Vendor Indexing
  VendorIndexingData = new BehaviorSubject<Array<number>>(new Array<number>());
  RouteType = new BehaviorSubject<string>('');
  HomeCategoryComponentFlag = new BehaviorSubject<boolean>(true);


  isForgotOpen = new BehaviorSubject<boolean>(false);
  isSubscribeOpen = new BehaviorSubject<boolean>(false);
  ChangePasswordOpen = new BehaviorSubject<boolean>(false);
  OtpInputShowHide = new BehaviorSubject<boolean>(false);
  CityName = new BehaviorSubject<string>('');

  constructor(private http: HttpClient,
    private toastr: ToastrService, private authService: SocialAuthService,
    private router: Router
  ) { }
  // Otp Input 
  GetCityName() {
    return this.CityName.asObservable();
  }
  SetCityName(value: any) {
    this.CityName.next(value);
  }

  // Otp Input 
  GetOtpInputShowHide() {
    return this.OtpInputShowHide.asObservable();
  }
  SetOtpInputShowHide(value: boolean) {
    this.OtpInputShowHide.next(value);
  }
  // Change Password
  GetChangePasswordOpen() {
    return this.ChangePasswordOpen.asObservable();
  }
  SetChangePasswordOpen(value: boolean) {
    this.ChangePasswordOpen.next(value);
  }
  // Forgot
  GetisForgotOpen() {
    return this.isForgotOpen.asObservable();
  }
  SetisForgotOpen(value: boolean) {
    this.isForgotOpen.next(value);
  }
  // Subscribe Modal
  GetisSubscribeOpen() {
    return this.isSubscribeOpen.asObservable();
  }
  SetisSubscribeOpen(value: boolean) {
    this.isSubscribeOpen.next(value);
  }
  // Banner
  GetHomeCategoryComponentFlag() {
    return this.HomeCategoryComponentFlag.asObservable();
  }
  SetHomeCategoryComponentFlag(value: boolean) {
    this.HomeCategoryComponentFlag.next(value);
  }
  // Section Route Type
  GetRouteType() {
    return this.RouteType.asObservable();
  }
  SetRouteType(data) {
    this.RouteType.next(data);
  }
  // Vendor Indexing
  GetVendorIndexingData() {
    return this.VendorIndexingData.asObservable();
  }
  SetVendorIndexingData(data) {
    this.VendorIndexingData.next(data);
  }
  // Banner
  GetVendor_Banner_Tempalte() {
    return this.Vendor_Banner_Tempalte.asObservable();
  }
  SetVendor_Banner_Tempalte(value: boolean) {
    this.Vendor_Banner_Tempalte.next(value);
  }
  // Banner
  GetBanner_Tempalte() {
    return this.Banner_Tempalte.asObservable();
  }
  SetBanner_Tempalte(value: boolean) {
    this.Banner_Tempalte.next(value);
  }
  // Search
  GetSearch_Input() {
    return this.Search_Input.asObservable();
  }
  SetSearch_Input(value) {
    this.Search_Input.next(value);
  }
  // Search
  GetSearch_Input_Filter() {
    return this.Search_Input_Filter.asObservable();
  }
  SetSearch_Input_Filter(value) {
    this.Search_Input_Filter.next(value);
  }


  GetPortFolio_PopUp() {
    return this.PortFolio_PopUp.asObservable();
  }
  SetPortFolio_PopUp(value: boolean) {
    this.PortFolio_PopUp.next(value);
  }



  GetActiveMenuLink() {
    return this.ActiveMenuLink.asObservable();
  }
  SetActiveMenuLink(data) {
    this.ActiveMenuLink.next(data);
  }
  GetVendorCategoryData() {
    return this.VendorCategoryData.asObservable();
  }
  SetVendorCategoryData(data) {
    this.VendorCategoryData.next(data);
  }
  GetVendorPortFolioPopUp() {
    return this.VendorPortFolioPopUp.asObservable();
  }
  SetVendorPortFolioPopUp(value: boolean) {
    this.VendorPortFolioPopUp.next(value);
  }
  GetVendorPortFolioPopUpList() {
    return this.VendorPortFolioPopUpList.asObservable();
  }
  SetVendorPortFolioPopUpList(value: boolean) {
    this.VendorPortFolioPopUpList.next(value);
  }
  getLoggedInStatusVendor() {
    return this.isVendorLoggedIn.asObservable();
  }

  setLoggedInStatusVendor(value) {
    this.isVendorLoggedIn.next(value)
  }
  GetReachOutPopUp() {
    return this.ReachOutPopUp.asObservable();
  }
  SetReachOutPopUp(value: boolean) {
    this.ReachOutPopUp.next(value);
  }
  GetReviewPopUp() {
    return this.ReviewPopUp.asObservable();
  }
  SetReviewPopUp(value: boolean) {
    this.ReviewPopUp.next(value);
  }
  getlocation(): Observable<any> {
    return this.http.get(`http://ip-api.com/json`)
      .pipe(
        map((res: Response) => res));
  }

  post(url, data) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(this.serverUrl + url, data, {
      headers: headers
    });
  }

  postFile(fileToUpload: File) {
    const formData: FormData = new FormData();
    formData.append('attachment', fileToUpload, fileToUpload.name);
    return this.http.post(this.serverUrl + 'upload_file', formData)
  }
  postData(url, data) {
    return this.http.post(this.serverUrl + url, data).pipe(catchError(this.handleError));
  };

  putData(url, data) {
    return this.http.put(this.serverUrl + url, data).pipe(catchError(this.handleError));
  };

  getData(url) {
    return this.http.get(this.serverUrl + url).pipe(catchError(this.handleError));
  }

  setTokenAndUserDetails(userDetails) {
    sessionStorage.setItem("Token", userDetails.Token);
    sessionStorage.setItem("UserDetails", JSON.stringify(userDetails));
  }

  getToken() {
    return sessionStorage.getItem("Token");
  }

  getUserDetails() {
    return sessionStorage.getItem("UserDetails");
  }

  showSuccess(message) {
    this.toastr.success(message);
  }

  showError(message) {
    this.toastr.error(message);
  }

  getLoggedInStatus() {
    return this.isLoggedIn.asObservable();
  }

  setLoggedInStatus(value) {
    this.isLoggedIn.next(value)
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      // errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      errorMessage = `${error.message}`;
    }
    // window.alert(errorMessage);
    return throwError(errorMessage);
  }

  signOut() {
    sessionStorage.removeItem('Token');
    sessionStorage.removeItem("UserDetails");
    this.setLoggedInStatus(false);
    this.router.navigate(['/']);
    this.authService.signOut();
  }

  getActiveVendorId() {
    return this.activeVendorId.asObservable();
  }

  setActiveVendorId(value) {
    this.activeVendorId.next(value)
  }

  setActiveVendors(activeVendor) {
    this.activeVendors.next(activeVendor)
  }

  getActiveVendors() {
    return this.activeVendors.asObservable();
  }

  setLogin(value) {
    this.isLoginOpen.next(value);
  }

  getLogin() {
    return this.isLoginOpen.asObservable();
  }

  setSignup(value) {
    this.isSignupOpen.next(value);
  }

  getSignup() {
    return this.isSignupOpen.asObservable();
  }

  setEditProfile(value) {
    this.isEditProfileOpen.next(value);
  }

  getEditProfile() {
    return this.isEditProfileOpen.asObservable();
  }

  setCalculatorData(calculatorDetails) {
    sessionStorage.setItem("calculatorDetails", JSON.stringify(calculatorDetails));
  }

  getCalculatorData() {
    return sessionStorage.getItem("calculatorDetails");
  }

  setApiCalculatorData(calculatorDetails) {
    sessionStorage.setItem("ApiCalculatorDetails", JSON.stringify(calculatorDetails));
  }

  getApiCalculatorData() {
    return sessionStorage.getItem("ApiCalculatorDetails");
  }

  setTopologyData(topology) {
    sessionStorage.setItem("add-topology", JSON.stringify(topology));
  }

  getTopologyData() {
    return sessionStorage.getItem("add-topology");
  }

  setTopographyData(topography) {
    sessionStorage.setItem("add-topography", JSON.stringify(topography));
  }

  getTopographyData() {
    return sessionStorage.getItem("add-topography");
  }
  setLayoutDetails(data) {
    sessionStorage.setItem("select-layout", JSON.stringify(data));
  }

  getLayoutDetails() {
    return sessionStorage.getItem("select-layout");
  }
  setBhkDetails(data) {
    sessionStorage.setItem("select-project", JSON.stringify(data));
  }

  getBhkDetails() {
    return sessionStorage.getItem("select-project");
  }
  destoryBhkDetails() {
    return sessionStorage.removeItem("select-project");
  }

  setCalculatorRouter(data) {
    sessionStorage.setItem("calculator-router", JSON.stringify(data));
  }

  getCalculatorRouter() {
    return sessionStorage.getItem("calculator-router");
  }
  destoryCalculatorRouter() {
    return sessionStorage.removeItem("calculator-router");
  }


  // setVendorLogin(value) {
  //   this.isVendorLoginOpen.next(value);
  // }

  // getVendorLogin() {
  //   return this.isVendorLoginOpen.asObservable();
  // }

  // setVendorSignup(value) {
  //   this.isVendorSignupOpen.next(value);
  // }

  // getVendorSignup() {
  //   return this.isVendorSignupOpen.asObservable();
  // }
}


