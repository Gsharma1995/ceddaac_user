import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { SharedService } from '../shared.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuarGuard implements CanActivate {
  constructor(private SharedService: SharedService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let useraDetails = this.SharedService.getUserDetails();
    if (useraDetails && useraDetails.length > 0) {
      return true;
    }
    else {
      this.SharedService.showError("Please login to proceed");
      return false;
    }
  }
}
