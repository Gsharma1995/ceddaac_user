import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import {AutocompleteLibModule} from 'angular-ng-autocomplete';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,AutocompleteLibModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 3500,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    })
  ],
  exports: []
})
export class SharedModule { }
