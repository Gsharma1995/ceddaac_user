import { Component, OnInit, Output, EventEmitter, ViewEncapsulation, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SharedService } from 'src/app/shared/shared.service';
import { Router } from '@angular/router';

import { SocialAuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { SocialUser } from "angularx-social-login";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ChangePasswordComponent implements OnInit {
  @Input() loginModule: string;
  loginForm: FormGroup;
  submitted: boolean;
  user: SocialUser;
  loggedIn: boolean;
  Form_cng: FormGroup; submittedcng = false; btnsubmittedcng = false;
  UserDetails:any=JSON.parse(sessionStorage.getItem('UserDetails'));
  constructor(private formBuilder: FormBuilder,
    private sharedService: SharedService,private authService: SocialAuthService,
    private router: Router) {
  }
  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }
  onLoginRedirect(){
    this.sharedService.SetisForgotOpen(false);
    this.sharedService.setLogin(true);
  }
  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(x => 
      console.log('x::::',x)
      );

  }
  
  get f_cng() { return this.Form_cng.controls; }
  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required]]
    });
    this.Form_cng = this.formBuilder.group({
      password: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(20)]],
      current_password: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(20)]],
    })
  }
  closemodel() {
    this.formReset();
    this.sharedService.setLogin(true);
    this.sharedService.SetChangePasswordOpen(false);
  }

  loginSubmit() {
    var self = this;
    self.submittedcng = true;
    if (self.Form_cng.invalid) {
      return;
    } else {
      this.btnsubmittedcng = true;
      var sendRequest = this.Form_cng.value;
      sendRequest.uid= this.UserDetails;
      self.sharedService.post('change_password', this.Form_cng.value).subscribe((res: any) => {
        if (!res.error) {
          if (res.replyCode == 'success') {
            this.sharedService.SetChangePasswordOpen(false);
            this.closemodel();
            self.sharedService.showSuccess(res.replyMsg);
            self.Form_cng.reset();
            self.submittedcng = false;
            self.btnsubmittedcng = false;
          } else {
            self.btnsubmittedcng = false;
            self.sharedService.showError(res.replyMsg)
          }
        } else {
          self.btnsubmittedcng = false;
          self.sharedService.showError(res.error.replyMsg)
        }
      }, error => {
        self.btnsubmittedcng = false;
        self.sharedService.showError("Oops! Something went wrong!");
      });
    }
  }

  formReset() {
    this.submitted = false;
    this.Form_cng.reset();
  }

  openSignupPopup() {
    this.closemodel();
    this.sharedService.setSignup(true);
  }

  openSignupVendorPopup() {
    this.closemodel();
    this.sharedService.setSignup(false);
    this.router.navigate(['/vendor-login'])
  }
}
