import { SharedService } from 'src/app/shared/shared.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common'
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
declare const $: any;

@Component({
    selector: 'app-vendor-portfolio',
    templateUrl: './add-vendor-portfolio.component.html',
    styleUrls: ['./add-vendor-portfolio.component.css'],
    providers: [DatePipe]
})
export class AddVendorPortfolioComponent implements OnInit {

    AddForm: FormGroup; Id: any; UserData: any;Layout_Id: any;
    submitted = false; btnsubmitted = false;
    flag: boolean;
    coordinates: any;
    ProductList:any;hProduct: string;
    FinishingList:any;hFinishing: string;

    image1FlagBackend: boolean = false; image1FlagFrontend: boolean = false; image1FlagSelect: boolean = false;
    url1: any = '';image1: any = "assets/images/no.jpg";

    image2FlagBackend: boolean = false; image2FlagFrontend: boolean = false; image2FlagSelect: boolean = false;
    url2: any = '';image2: any = "assets/images/no.jpg";

    image3FlagBackend: boolean = false; image3FlagFrontend: boolean = false; image3FlagSelect: boolean = false;
    url3: any = '';image3: any = "assets/images/no.jpg";

    image4FlagBackend: boolean = false; image4FlagFrontend: boolean = false; image4FlagSelect: boolean = false;
    url4: any = '';image4: any = "assets/images/no.jpg";

    image5FlagBackend: boolean = false; image5FlagFrontend: boolean = false; image5FlagSelect: boolean = false;
    url5: any = '';image5: any = "assets/images/no.jpg";
    Vendor_Flag_Popup:boolean=false;
    constructor(
        public fb: FormBuilder,
        public _employeeService: SharedService,
        public activatedRoute: ActivatedRoute,
        public router: Router, public sanitizer: DomSanitizer
    ) {
        this._employeeService.GetVendorPortFolioPopUp().subscribe(data =>{
            this.Vendor_Flag_Popup=true;
        })



        this.activatedRoute.params.forEach((urlParams) => {
            this.Id = urlParams['id'];
            this.Layout_Id = urlParams['layout_id_id'];
        });
        this.UserData = JSON.parse(sessionStorage.getItem('user'))
    }
    get f() { return this.AddForm.controls; }

    ngOnInit(): void {
        this.hProduct = '';
        this.getProductList();
        this.hFinishing = '';
        this.getFinishingList();
        if (this.Layout_Id != undefined) {
            this.flag = true;
            this.image1FlagBackend = true;
            this.image2FlagBackend = true;
            this.image3FlagBackend = true;
            this.image4FlagBackend = true;
            this.image5FlagBackend = true;

            this.image1FlagFrontend = false;
            this.image2FlagFrontend = false;
            this.image3FlagFrontend = false;
            this.image4FlagFrontend = false;
            this.image5FlagFrontend = false;
            this.AddForm = this.fb.group({
                id: [''],
                vendor_id: ['', [Validators.required]],
                template_no: ['', [Validators.required]],
                page_no: ['', [Validators.required]],
                label_name: ['', [Validators.required]],
                lable_text: ['', [Validators.required]],
                description: ['', [Validators.required]],
                image1: new FormControl(''),
                image2: new FormControl(''),
                image3: new FormControl(''),
                image4: new FormControl(''),
                image5: new FormControl(''),
                created_by: ['', [Validators.required]],
                modified_by: ['', [Validators.required]],
                view_type: ['', [Validators.required]],
            })
            this.getLoadList(this.Layout_Id);
        } else {
            this.flag = false;
            this.image1FlagBackend = false;
            this.image2FlagBackend = false;
            this.image3FlagBackend = false;
            this.image4FlagBackend = false;
            this.image5FlagBackend = false;

            this.image1FlagFrontend = true;
            this.image2FlagFrontend = true;
            this.image3FlagFrontend = true;
            this.image4FlagFrontend = true;
            this.image5FlagFrontend = true;
            this.AddForm = this.fb.group({
                vendor_id: ['', [Validators.required]],
                template_no: ['', [Validators.required]],
                page_no: ['', [Validators.required]],
                label_name: ['', [Validators.required]],
                lable_text: ['', [Validators.required]],
                description: ['', [Validators.required]],
                image1: new FormControl(''),
                image2: new FormControl(''),
                image3: new FormControl(''),
                image4: new FormControl(''),
                image5: new FormControl(''),
                created_by: ['', [Validators.required]],
                modified_by: ['', [Validators.required]],
                view_type: ['', [Validators.required]],
            })
        }
        this.UserData = JSON.parse(this._employeeService.getUserDetails());
        this.AddForm.controls['vendor_id'].setValue(this.UserData.id);
        this.AddForm.controls['created_by'].setValue(this.UserData.id);
        this.AddForm.controls['modified_by'].setValue(this.UserData.id);
    }
    getProductList(){
        var self = this;
        var sendRequestData = {id: self.Id}
        self._employeeService.post('type_of_project_list', sendRequestData).subscribe((res: any) => {
            if (res.replyCode == 'success') {
                self.ProductList=res.data;
            }
        },error => {
        });
    }
    getFinishingList(){
        var self = this;
        var sendRequestData = {id: self.Id}
        self._employeeService.post('finishing_list', sendRequestData).subscribe((res: any) => {
            if (res.replyCode == 'success') {
                self.FinishingList=res.data;
            }
        },error => {
        });
    }
    getLoadList(layout_id) {
        var self = this;
        var sendRequestData = { id: layout_id }
        self._employeeService.post('vendors_portfolio_details', sendRequestData).subscribe((res: any) => {
            if (res.replyCode == 'success') {
                this.AddForm.controls['id'].setValue(res.data.id);
                this.AddForm.controls['template_no'].setValue(res.data.template_no);
                this.AddForm.controls['page_no'].setValue(res.data.page_no);
                this.AddForm.controls['label_name'].setValue(res.data.label_name);
                this.AddForm.controls['lable_text'].setValue(res.data.lable_text);
                this.AddForm.controls['description'].setValue(res.data.description);
                this.AddForm.controls['view_type'].setValue(res.data.view_type);
                this.image1= res.data.image1;
                this.image2= res.data.image2;
                this.image3= res.data.image3;
                this.image4= res.data.image4;
                this.image5= res.data.image5;
                if(this.image1 == ''){
                    this.image1FlagBackend = false;
                    this.image1FlagFrontend = true;    
                }else{
                    this.dataURLtoFile(res.data.image1,'image1');
                }
                if(this.image2 == ''){
                    this.image2FlagBackend = false;
                    this.image2FlagFrontend = true;    
                }else{
                    // alert(res.data.image2)
                    this.dataURLtoFile(res.data.image2,'image2');
                }
                if(this.image3 == ''){
                    this.image3FlagBackend = false;
                    this.image3FlagFrontend = true;    
                }else{
                    this.dataURLtoFile(res.data.image3,'image3');
                }
                if(this.image4 == ''){
                    this.image4FlagBackend = false;
                    this.image4FlagFrontend = true;    
                }else{
                    this.dataURLtoFile(res.data.image4,'image4');
                }
                if(this.image5 == ''){
                    this.image5FlagBackend = false;
                    this.image5FlagFrontend = true;    
                }else{
                    this.dataURLtoFile(res.data.image5,'image5');
                }
            }
        }, error => {
        });
    }
    dataURLtoFile(dataurl,type) {
        const arr = dataurl.split('.');
        const imageExtension = arr[1];
        const bstr = atob(arr[1]);
        let n = bstr.length;
        const u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        let uploadedImage = new File([u8arr], `${arr[0]}.${imageExtension}`);
        if(type == 'image1'){
            this.AddForm.controls['image1'].setValue(uploadedImage ? uploadedImage.name : '', { emitModelToViewChange: false });
        }else if(type == 'image2'){
            this.AddForm.controls['image2'].setValue(uploadedImage ? uploadedImage.name : '', { emitModelToViewChange: false });
        }else if(type == 'image3'){
            this.AddForm.controls['image3'].setValue(uploadedImage ? uploadedImage.name : '', { emitModelToViewChange: false });
        }else if(type == 'image4'){
            this.AddForm.controls['image4'].setValue(uploadedImage ? uploadedImage.name : '', { emitModelToViewChange: false });
        }else if(type == 'image5'){
            this.AddForm.controls['image5'].setValue(uploadedImage ? uploadedImage.name : '', { emitModelToViewChange: false });
        }
    }

    allowurl(url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
    onSelectFile(file,type) {
        var reader = new FileReader();
            reader.readAsDataURL(file);
        if(type == 'image1'){
            reader.onload = (event) => {
                this.url1 = event.target.result;
                this.image1FlagSelect = true;
                this.image1FlagFrontend = false;
                this.image1FlagBackend = false;
            }
            this._employeeService.postFile(file).subscribe((data: any) => {
                this.AddForm.controls['image1'].setValue(data.name ? data.name : '', { emitModelToViewChange: false });
            }, error => {
            });
        }else if(type == 'image2'){
            reader.onload = (event) => {
                this.url2 = event.target.result;
                this.image2FlagSelect = true;
                this.image2FlagFrontend = false;
                this.image2FlagBackend = false;
            }
            this._employeeService.postFile(file).subscribe((data: any) => {
                this.AddForm.controls['image2'].setValue(data.name ? data.name : '', { emitModelToViewChange: false });
            }, error => {
            });
        }else if(type == 'image3'){
            reader.onload = (event) => {
                this.url3 = event.target.result;
                this.image3FlagSelect = true;
                this.image3FlagFrontend = false;
                this.image3FlagBackend = false;
            }
            this._employeeService.postFile(file).subscribe((data: any) => {
                this.AddForm.controls['image3'].setValue(data.name ? data.name : '', { emitModelToViewChange: false });
            }, error => {
            });
        }else if(type == 'image4'){
            reader.onload = (event) => {
                this.url4 = event.target.result;
                this.image4FlagSelect = true;
                this.image4FlagFrontend = false;
                this.image4FlagBackend = false;
            }
            this._employeeService.postFile(file).subscribe((data: any) => {
                this.AddForm.controls['image4'].setValue(data.name ? data.name : '', { emitModelToViewChange: false });
            }, error => {
            });
        }else if(type == 'image5'){
            reader.onload = (event) => {
                this.url5 = event.target.result;
                this.image5FlagSelect = true;
                this.image5FlagFrontend = false;
                this.image5FlagBackend = false;
            }
            this._employeeService.postFile(file).subscribe((data: any) => {
                this.AddForm.controls['image5'].setValue(data.name ? data.name : '', { emitModelToViewChange: false });
            }, error => {
            });
        }
    }
    onSubmit() {
        var self = this;
        self.submitted = true;
        var sendRequestData = self.AddForm.value;
        if (self.AddForm.invalid) {
            return;
        } else {
            self.btnsubmitted = true;
            var sendRequestData = self.AddForm.value;
            // alert('success');
            self._employeeService.post('add_vendors_portfolio', sendRequestData).subscribe((res: any) => {
                if (!res.error) {
                    if (res.replyCode == 'success') {
                        // this.router.navigate(['/vendor-portfolio/view-vendor-portfolio/', this.Id]);
                        self._employeeService.showSuccess(res.replyMsg)
                        self.AddForm.reset();
                        self.submitted = false;
                        self.btnsubmitted = false;
                    } else {
                        self.btnsubmitted = false;
                        self._employeeService.showError(res.replyMsg)
                    }
                } else {
                    self.btnsubmitted = false;
                    self._employeeService.showError(res.error.replyMsg)
                }
            }, error => {
                self.btnsubmitted = false;
                self._employeeService.showError("Oops! Something went wrong!");
            });
        }
    }
    onCancel() {
        this.router.navigate(['/vendor-portfolio/view-vendor-portfolio/', this.Id]);
    }
    closeditProfileVendor(){
        this._employeeService.SetVendorPortFolioPopUp(false);
      }
}


