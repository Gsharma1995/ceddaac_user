import { SharedService } from 'src/app/shared/shared.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

declare const $: any;
@Component({
    selector: 'app-view-vendor-portfolio',
    templateUrl: './view-vendor-portfolio.component.html',
    styleUrls: ['./view-vendor-portfolio.component.css']
})
export class ViewVendorPortfolioComponent implements OnInit {
    user: any; 
    status = [{ type: 'active', label: 'Success' }, { type: 'inactive', label: 'Pending' }];
    @ViewChild('OpenImageModal', { static: false }) public OpenImageModal;
    data: any; keys: any; record_flag: boolean = false;
    ImageFlag: boolean;
    selectedImage: any; noselectedImage: any; selectedImageFlag: boolean = false; noselectedImageFlag: boolean = false;
    id: any;
    // Search Form
    searchForm: FormGroup;
    // Testing Data
    LocalData: any = [
        { id: 1, name: 'test1', image: 'attachment-1598341487819.png', description: 'description1' },
        { id: 2, name: 'test2', image: '', description: 'description2' },
        { id: 3, name: 'test3', image: 'attachment-1598341487819.png', description: 'description3' },
        { id: 4, name: 'test4', image: 'attachment-1598341487819.png', description: 'description4' }
    ];
    @ViewChild('updateStatusModal', { static: false }) public updateStatusModal;
    @ViewChild('deleteModal', { static: false }) public deleteModal;
    bodyData: any;Id:any;
    Loader_Flag:boolean=true;
    UserData:any;
    constructor(
        private router: Router,
        public activatedRoute: ActivatedRoute,
        public _sharedService: SharedService,
        private fb: FormBuilder,
        private sanitizer: DomSanitizer
    ) {
        this.activatedRoute.params.forEach((urlParams) => {
            this.Id = urlParams['id'];
        });
        this.user = JSON.parse(sessionStorage.getItem('user'))
    }
    ngOnInit(): void {
        this.searchForm = this.fb.group({
            title: [''],
            status: [],
            id: []
        })
        this.getLoadList(this.Id);
    }
    resetfilter() {
        this.searchForm.reset();
        this.getLoadList(this.Id);
    }
    getLoadList(id) {
        var self = this;
        this.UserData = JSON.parse(this._sharedService.getUserDetails());
        var requestData = { "vendor_id": this.UserData.id }
        self.Loader_Flag=true;
        self.record_flag = false;
        self._sharedService.post('vendors_portfolio_list',requestData).subscribe((res: any) => {
            if (!res.error) {
                self.Loader_Flag = false;
                if (res.data.length > 0) {
                    self.record_flag = true;
                    // Local Data
                    // self.data = self.LocalData;
                    self.data = res.data;
                    self.keys = Object.keys(self.data[0]);
                } else {
                    self.record_flag = false;
                }
                // self._sharedService.showSuccess(res.replyMsg);
                // this.router.navigateByUrl('/admin/shop-list');
            }
        }, error => {
            this.Loader_Flag = false;
            self.record_flag = false;
        });
    }
    allowurl(url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
    showImageModal(image) {
        if (image == '') {
            this.noselectedImageFlag = true;
            this.selectedImageFlag = false;
            this.noselectedImage = 'assets/images/no.jpg';
            this.OpenImageModal.show();
        } else {
            this.selectedImageFlag = true;
            this.noselectedImageFlag = false;
            this.selectedImage = image;
            this.OpenImageModal.show();
        }
    }
    oneditAction(id) {
        this.router.navigate(['/vendor-portfolio/edit-vendor-portfolio/', this.Id, id]);
    }
    onaddAction() {
        this.router.navigate(['/vendor-portfolio/add-vendor-portfolio/', this.Id])
    }
    ondeleteAction(id) {
        var self = this;
        // this._sharedService.loadingShow = true;
        var sendRequestData = { "id": id, "status": 2 }
        self._sharedService.post('update_vendors_portfolio_status', sendRequestData).subscribe((res: any) => {
            if (!res.error) {
                // this._sharedService.loadingShow = false;
                this._sharedService.showSuccess(res.replyMsg);
                self.getLoadList(this.Id);
            } else {
                // this._sharedService.loadingShow = false;
                this._sharedService.showError("Oops! Something went wrong!");
            }
        }, error => {
            // this._sharedService.loadingShow = false;

            this._sharedService.showError("Oops! Something went wrong!");
        });
    }
    updateStatusData(event) {
        this.bodyData = event;
        this.updateStatusModal.show();
    }
    updateStatus() {
        var self = this;
        var value = this.bodyData;
        var status;
        if (value.status == '1') {
            status = '0';
        }
        else {
            status = '1';
        }
        var sendRequestData = {
            "id": this.bodyData.id,
            "status": status
        }
        self._sharedService.post('update_vendors_portfolio_status', sendRequestData).subscribe((res: any) => {
            if (!res.error) {
                this.updateStatusModal.hide();
                self.getLoadList(this.Id);
            }
        }, error => {
        });
    }
    gotoDelete(id) {
        this.deleteModal.show();
        this.id = id
    }
    deleteRecord() {
        var self = this;
        // this._sharedService.loadingShow = true;
        var sendRequestData = {
            "id": self.id,
            "status": 2
        }
        self._sharedService.post('update_vendors_portfolio_status', sendRequestData).subscribe((res: any) => {
            if (!res.error) {
                // this._sharedService.loadingShow = false;
                this.deleteModal.hide();
                this._sharedService.showSuccess(res.replyMsg);
                self.getLoadList(this.Id);
            }
            else {
                // this._sharedService.loadingShow = false;
                this._sharedService.showError("Oops! Something went wrong!");
            }
        }, error => {
            // this._sharedService.loadingShow = false;
            this._sharedService.showError("Oops! Something went wrong!");
        });
    }
    onBack(){
        this.router.navigate(['/vendor/view-vendor']);
    }
    closeditProfileVendor(){
        this._sharedService.SetVendorPortFolioPopUpList(false);
    }
}