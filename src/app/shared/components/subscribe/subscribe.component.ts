import { Component, OnInit, Output, EventEmitter, ViewEncapsulation, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SharedService } from 'src/app/shared/shared.service';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
declare const $: any;

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SubscribeComponent implements OnInit {
  @Input() loginModule: string;
  subscribeForm: FormGroup;
  submitted: boolean;
  loggedIn: boolean;
  constructor(private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private router: Router) {
  }
 
  ngOnInit(): void {
    this.subscribeForm = this.formBuilder.group({
      email: ['', [Validators.required]]
    });
  }

  get f() { return this.subscribeForm.controls; }

  onClose(){
    this.sharedService.SetisSubscribeOpen(false);
  }

  loginSubmit() {
    this.submitted = true;
    if (this.subscribeForm.valid) {
      this.sharedService.post("subscribe_newsletter", this.subscribeForm.value).subscribe((response: any) => {
        if(response.replyCode == 'success'){
          this.sharedService.showSuccess(response.replyMsg);
          this.sharedService.SetisForgotOpen(false);
          this.onClose();
        }else{
          this.sharedService.showError(response.replyMsg);
        }
      },err =>{
        this.sharedService.showError(err);
      });
    }
  };

  formReset() {
    this.submitted = false;
    this.subscribeForm.reset();
  }

  openSignupPopup() {
    this.onClose();
    this.sharedService.setSignup(true);
  }

  openSignupVendorPopup() {
    this.onClose();
    this.sharedService.setSignup(false);
    this.router.navigate(['/vendor-login'])
  }
}
