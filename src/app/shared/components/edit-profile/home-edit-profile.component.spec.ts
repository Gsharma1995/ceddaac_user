import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeEditProfileComponent } from './home-edit-profile.component';

describe('HomeEditProfileComponent', () => {
  let component: HomeEditProfileComponent;
  let fixture: ComponentFixture<HomeEditProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeEditProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeEditProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
