import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { SharedService } from 'src/app/shared/shared.service';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-home-edit-profile',
  templateUrl: './home-edit-profile.component.html',
  styleUrls: ['./home-edit-profile.component.css']
})
export class HomeEditProfileComponent implements OnInit {
  editProfileForm: FormGroup;
  editProfileSubmitted: boolean;
  userId = 0;
  fileToUpload: File = null;
  imageError: string;

  imageSrc: string = '';
  UserData: any;
  // Profile
  uploadedImagesData: any;
  imageFlagBackend: boolean = false;
  imageFlagFrontend: boolean = false;
  imageFlagSelect: boolean = false;
  url: any = '';
  @ViewChild('file', { static: false }) file: ElementRef;
  image: any = "assets/images/no.jpg";
  // Json_Type=JSON.parse(sessionStorage.getItem('user_type'));
  Vendor_Flag: boolean = false;
  User_Flag: boolean = false;
  constructor(private formBuilder: FormBuilder,
    public sharedService: SharedService,
    private http: HttpClient,
    public sanitizer: DomSanitizer) { }
  AddForm: FormGroup;
  submitted = false; btnsubmitted = false;

  // Profile

  imagevalidationflag: boolean = false;
  Selectimageflag: boolean;

  company_logoimageFlagBackend: boolean = false; company_logoimageFlagFrontend: boolean = false; company_logoimageFlagSelect: boolean = false;
  company_logourl: any = ''; company_logoimage: any = "assets/images/no.jpg";
  insurance_attachmentimageFlagBackend: boolean = false; insurance_attachmentimageFlagFrontend: boolean = false; insurance_attachmentimageFlagSelect: boolean = false;
  insurance_attachmenturl: any = ''; insurance_attachmentimage: any = "assets/images/no.jpg";
  licence_attachmentimageFlagBackend: boolean = false; licence_attachmentimageFlagFrontend: boolean = false; licence_attachmentimageFlagSelect: boolean = false;
  licence_attachmenturl: any = ''; licence_attachmentimage: any = "assets/images/no.jpg";
  CategoryList: any; hCategory: string;
  CityList: any; hCity: string;
  StateList: any; hState: string;
  CountryList: any; hCountry: string;
  ngOnInit(): void {
    if (sessionStorage.getItem('user_type') == 'vendor') {
      this.Vendor_Flag = true;
      this.hCategory = ''; this.hCity = ''; this.hState = ''; this.hCountry = '';
      this.getCategoryList(); this.getCountryList(); this.getStateList(); this.getCityList();
      this.AddForm = this.formBuilder.group({
        id: [''],
        company_name: ['', [Validators.required]],
        description: ['', [Validators.required]],
        vendor_type: ['', [Validators.required]],
        country_id: ['', [Validators.required]],
        state_id: ['', [Validators.required]],
        city_id: ['', [Validators.required]],
        pincode: ['', [Validators.required]],
        company_logo: new FormControl('', [Validators.required]),
        logo_type: ['', [Validators.required]],
        primary_contact_no: ['', [Validators.required]],
        website: ['', [Validators.required]],
        poc_name: ['', [Validators.required]],
        poc_title: ['', [Validators.required]],
        poc_number: ['', [Validators.required]],
        poc_email: ['', [Validators.required]],
        general_details: ['', [Validators.required]],
        date_ce: ['', [Validators.required]],
        geo_service_area: ['', [Validators.required]],
        gross_annual: ['', [Validators.required]],
        business_type: ['', [Validators.required]],
        insurance: ['', [Validators.required]],
        insurance_type: ['', [Validators.required]],
        insurance_attachment: new FormControl('', [Validators.required]),
        licence: ['', [Validators.required]],
        licence_type: ['', [Validators.required]],
        licence_attachment: new FormControl('', [Validators.required]),
        additional_info: ['', [Validators.required]],
        bank_name: ['', [Validators.required]],
        bank_account_no: ['', [Validators.required]],
        ifsc_code: ['', [Validators.required]],
        bank_address: ['', [Validators.required]],
        certification_name: ['', [Validators.required]],
        certification_place: ['', [Validators.required]],
        certification_date: ['', [Validators.required]],
        address: ['', [Validators.required]],
        // created_by: ['', [Validators.required]],
      })
      this.getLoadSessionVendorData();
    } else {
      this.Vendor_Flag = false;
      this.User_Flag = true;
      this.imageFlagBackend = true;
      this.imageFlagFrontend = false;
      this.editProfileForm = this.formBuilder.group({
        id: [''],
        first_name: ['', Validators.required],
        last_name: ['', Validators.required],
        email: ['', Validators.required],
        mobile: ['', Validators.required],
        image: new FormControl('', [Validators.required]),
      });
      if (this.sharedService.getUserDetails()) {
        this.getLoadSessionData();
      }
    }
  }
  getCategoryList() {
    var self = this;
    // var sendRequestData = { id: self.Id }
    // self.sharedService.post('category_list', sendRequestData).subscribe((res: any) => {
    //     if (res.replyCode == 'success') {
    //         self.CategoryList=res.data;
    //     }
    // }, error => {
    // });
  }
  getCountryList() {
    var self = this;
    // var sendRequestData = { id: self.Id }
    self.sharedService.post('country_drop_down_list', {}).subscribe((res: any) => {
      if (res.replyCode == 'success') {
        self.CountryList = res.data;
      }
    }, error => {
    });
  }
  getStateList() {
    var self = this;
    // var sendRequestData = { id: self.Id }
    self.sharedService.post('state_drop_down_list', {}).subscribe((res: any) => {
      if (res.replyCode == 'success') {
        self.StateList = res.data;
      }
    }, error => {
    });
  }
  getCityList() {
    var self = this;
    // var sendRequestData = { id: self.Id }
    self.sharedService.post('city_drop_down_list', {}).subscribe((res: any) => {
      if (res.replyCode == 'success') {
        self.CityList = res.data;
      }
    }, error => {
    });
  }
  get s() { return this.editProfileForm.controls; }
  getLoadSessionData() {
    this.imageFlagBackend = true;
    this.imageFlagFrontend = false;
    this.UserData = JSON.parse(this.sharedService.getUserDetails());
    this.editProfileForm.controls['first_name'].setValue(this.UserData.first_name);
    this.editProfileForm.controls['last_name'].setValue(this.UserData.last_name);
    this.editProfileForm.controls['email'].setValue(this.UserData.email);
    this.editProfileForm.controls['id'].setValue(this.UserData.id);
    this.editProfileForm.controls['mobile'].setValue(this.UserData.mobile);
    this.image = this.UserData.image;
    // if(this.image == ''){
    this.imageFlagBackend = true;
    this.imageFlagFrontend = false;
    // }else{
    this.dataURLtoFile(this.UserData.image);
    // }
  }
  
  get f() { return this.AddForm.controls; }
  getLoadSessionVendorData() {
    this.UserData = JSON.parse(this.sharedService.getUserDetails());
    this.AddForm.controls['id'].setValue(this.UserData.id);
    this.AddForm.controls['company_name'].setValue(this.UserData.company_name);
    this.AddForm.controls['description'].setValue(this.UserData.description);
    this.AddForm.controls['vendor_type'].setValue(this.UserData.vendor_type);
    this.AddForm.controls['country_id'].setValue(this.UserData.country_id);
    this.AddForm.controls['state_id'].setValue(this.UserData.state_id);
    this.AddForm.controls['city_id'].setValue(this.UserData.city_id);
    this.AddForm.controls['pincode'].setValue(this.UserData.pincode);
    this.AddForm.controls['logo_type'].setValue(this.UserData.logo_type);
    this.AddForm.controls['primary_contact_no'].setValue(this.UserData.primary_contact_no);
    this.AddForm.controls['website'].setValue(this.UserData.website);
    this.AddForm.controls['poc_name'].setValue(this.UserData.poc_name);
    this.AddForm.controls['poc_title'].setValue(this.UserData.poc_title);
    this.AddForm.controls['poc_number'].setValue(this.UserData.poc_number);
    this.AddForm.controls['poc_email'].setValue(this.UserData.poc_email);
    this.AddForm.controls['general_details'].setValue(this.UserData.general_details);
    this.AddForm.controls['geo_service_area'].setValue(this.UserData.geo_service_area);
    this.AddForm.controls['gross_annual'].setValue(this.UserData.gross_annual);
    this.AddForm.controls['business_type'].setValue(this.UserData.business_type);
    this.AddForm.controls['insurance'].setValue(this.UserData.insurance);
    this.AddForm.controls['insurance_type'].setValue(this.UserData.insurance_type);
    this.AddForm.controls['licence'].setValue(this.UserData.licence);
    this.AddForm.controls['licence_type'].setValue(this.UserData.licence_type);
    this.AddForm.controls['additional_info'].setValue(this.UserData.additional_info);
    this.AddForm.controls['bank_name'].setValue(this.UserData.bank_name);
    this.AddForm.controls['bank_account_no'].setValue(this.UserData.bank_account_no);
    this.AddForm.controls['ifsc_code'].setValue(this.UserData.ifsc_code);
    this.AddForm.controls['bank_address'].setValue(this.UserData.bank_address);
    this.AddForm.controls['certification_name'].setValue(this.UserData.certification_name);
    this.AddForm.controls['certification_place'].setValue(this.UserData.certification_place);
    this.AddForm.controls['address'].setValue(this.UserData.address);
    // this.AddForm.controls['created_by'].setValue(this.UserData.created_by);

    var certification_date = new Date(this.UserData.certification_date);
    var date_ce = new Date(this.UserData.date_ce);
    this.AddForm.controls['date_ce'].setValue(date_ce.toISOString().split('T')[0]);
    this.AddForm.controls['certification_date'].setValue(certification_date.toISOString().split('T')[0]);
    // this.dataURLtoFileVendor(this.UserData.company_logo, 'company_logo');
    // this.dataURLtoFileVendor(this.UserData.insurance_attachment, 'insurance_attachment');
    // this.dataURLtoFileVendor(this.UserData.licence_attachment, 'licence_attachment');

    this.company_logoimage = this.UserData.company_logo;
    this.insurance_attachmentimage = this.UserData.insurance_attachment;
    this.licence_attachmentimage = this.UserData.licence_attachment;

    



    if(this.company_logoimage == null || this.company_logoimage == undefined){
      this.company_logoimageFlagFrontend = true;
      this.company_logoimageFlagBackend = false;
    }else{
      this.dataURLtoFileVendor(this.UserData.company_logo, 'company_logo');
      this.company_logoimageFlagFrontend = false;
      this.company_logoimageFlagBackend = true;
    }
    if(this.insurance_attachmentimage == null || this.insurance_attachmentimage == undefined){
      this.insurance_attachmentimageFlagBackend = false;
      this.insurance_attachmentimageFlagFrontend = true;
    }else{
      this.insurance_attachmentimageFlagBackend = true;
      this.insurance_attachmentimageFlagFrontend = false;
      this.dataURLtoFileVendor(this.UserData.insurance_attachment, 'insurance_attachment');
    }
    if(this.licence_attachmentimage == null || this.licence_attachmentimage == undefined){
      this.licence_attachmentimageFlagBackend = false;
      this.licence_attachmentimageFlagFrontend = true;
    }else{
      this.licence_attachmentimageFlagBackend = true;
      this.licence_attachmentimageFlagFrontend = false;
      this.dataURLtoFileVendor(this.UserData.licence_attachment, 'licence_attachment');
    }
  }
  dataURLtoFileVendor(dataurl, type) {
    const arr = dataurl.split('.');
    const imageExtension = arr[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    let uploadedImage = new File([u8arr], `${arr[0]}.${imageExtension}`);
    if (type == 'company_logo') {
      this.AddForm.controls['company_logo'].setValue(uploadedImage ? uploadedImage.name : '', { emitModelToViewChange: false });
    } else if (type == 'insurance_attachment') {
      this.AddForm.controls['insurance_attachment'].setValue(uploadedImage ? uploadedImage.name : '', { emitModelToViewChange: false });
    } else if (type == 'licence_attachment') {
      this.AddForm.controls['licence_attachment'].setValue(uploadedImage ? uploadedImage.name : '', { emitModelToViewChange: false });
    }
  }





  uploadFileToActivityVendor(image) {
    this.sharedService.postFile(image).subscribe((data: any) => {
      this.uploadedImagesData = data.name;
      this.AddForm.controls['company_logo'].setValue(this.uploadedImagesData ? this.uploadedImagesData : '', { emitModelToViewChange: false });
    }, error => {
    });
  }





  onSelectVendorFile(file, type) {
    if (type == 'company_logo') {
      this.sharedService.postFile(file).subscribe((data: any) => {
        this.uploadedImagesData = data.name;
        this.AddForm.controls['company_logo'].setValue(this.uploadedImagesData ? this.uploadedImagesData : '', { emitModelToViewChange: false });
      }, error => {
      });
    } else if (type == 'insurance_attachment') {
      this.sharedService.postFile(file).subscribe((data: any) => {
        this.uploadedImagesData = data.name;
        this.AddForm.controls['insurance_attachment'].setValue(this.uploadedImagesData ? this.uploadedImagesData : '', { emitModelToViewChange: false });
      }, error => {
      });
    } else if (type == 'licence_attachment') {
      this.sharedService.postFile(file).subscribe((data: any) => {
        this.uploadedImagesData = data.name;
        this.AddForm.controls['licence_attachment'].setValue(this.uploadedImagesData ? this.uploadedImagesData : '', { emitModelToViewChange: false });
      }, error => {
      });
    }
    // this.uploadFileToActivity(file)
    if (file) {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      if (type == 'company_logo') {
        reader.onload = (event) => {
          this.company_logourl = event.target.result;
          this.company_logoimageFlagSelect = true;
          this.company_logoimageFlagFrontend = false;
          this.company_logoimageFlagBackend = false;
        }
      } else if (type == 'insurance_attachment') {
        reader.onload = (event) => {
          this.insurance_attachmenturl = event.target.result;
          this.insurance_attachmentimageFlagSelect = true;
          this.insurance_attachmentimageFlagFrontend = false;
          this.insurance_attachmentimageFlagBackend = false;
        }
      } else if (type == 'licence_attachment') {
        reader.onload = (event) => {
          this.licence_attachmenturl = event.target.result;
          this.licence_attachmentimageFlagSelect = true;
          this.licence_attachmentimageFlagFrontend = false;
          this.licence_attachmentimageFlagBackend = false;
        }
      }
    }
  }
  closeditProfile() {
    this.editProfileReset();
    this.sharedService.setEditProfile(false);
  }
  dataURLtoFile(dataurl) {
    const arr = dataurl.split('.');
    const imageExtension = arr[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    let uploadedImage = new File([u8arr], `${arr[0]}.${imageExtension}`);
    this.editProfileForm.controls['image'].setValue(uploadedImage ? uploadedImage.name : '', { emitModelToViewChange: false });
  }
  uploadFileToActivity(image) {
    this.sharedService.postFile(image).subscribe((data: any) => {
      this.uploadedImagesData = data.name;
      this.editProfileForm.controls['image'].setValue(this.uploadedImagesData ? this.uploadedImagesData : '', { emitModelToViewChange: false });
    }, error => {
    });
  }
  allowurl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  onSelectFile(file) {
    this.uploadFileToActivity(file);
    if (file) {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (event) => {
        this.url = event.target.result;
        this.imageFlagSelect = true;
        this.imageFlagFrontend = false;
        this.imageFlagBackend = false;
      }
    }
  }

  editProfileSubmit() {

    this.editProfileSubmitted = true;
    var sendRequestData = this.editProfileForm.value;
    this.sharedService.postData("register", sendRequestData).subscribe((response: any) => {
      if (response.replyCode == 'success') {
        this.UserData.first_name = sendRequestData.first_name;
        this.UserData.last_name = sendRequestData.last_name;
        this.UserData.email = sendRequestData.email;
        this.UserData.mobile = sendRequestData.mobile;
        this.UserData.image = sendRequestData.image;
        this.sharedService.setTokenAndUserDetails(this.UserData);
        this.editProfileSubmitted = false;
        this.sharedService.showSuccess(response.replyMsg);
      } else {
        this.sharedService.showError(response.replyMsg);
      }
    });
  }
  editProfileSubmit1() {
    this.editProfileSubmitted = true;
  }

  editProfileReset() {
    this.editProfileSubmitted = false;
    this.editProfileForm.reset();
  }

  handleFileInput(files: FileList) {
    const max_size = 2097152;
    const allowed_types = ['image/png', 'image/jpeg', 'image/jpeg'];
    this.imageError = '';
    if (files.item(0).size > max_size) {
      this.imageError = 'Maximum size allowed is 2Mb';
      return false;
    }
    else if (!allowed_types.includes(files.item(0).type)) {
      this.imageError = 'Only Images are allowed ( JPG | PNG )';
      return false;
    }
    else {
      this.fileToUpload = files.item(0);
    }
  }




  // Vendor
  closeditProfileVendor(){
    
    this.sharedService.setEditProfile(false);
  }
  onSubmit() {
    var self = this;
    self.submitted = true;
    if (self.AddForm.invalid) {
        return;
    } else {
        self.btnsubmitted = true;
        var sendRequestData = self.AddForm.value;
        self.sharedService.post('update_vendor_profile', sendRequestData).subscribe((res: any) => {
            if (!res.error) {
                if (res.replyCode == 'success') {
                  sessionStorage.setItem('UserDetails',JSON.stringify(sendRequestData))
                    // self.router.navigate(['/vendor/view-vendor'])
                    this.getLoadSessionVendorData();
                    self.sharedService.showSuccess(res.replyMsg)
                    self.AddForm.reset();
                    self.submitted = false;
                    self.btnsubmitted = false;
                } else {
                    self.btnsubmitted = false;
                    self.sharedService.showError(res.replyMsg)
                }
            } else {
                self.btnsubmitted = false;
                self.sharedService.showError(res.error.replyMsg)
            }
        }, error => {
            self.btnsubmitted = false;
            self.sharedService.showError("Oops! Something went wrong!");
        });
    }
}
onBack() {
    // this.router.navigate(['/vendor/view-vendor']);
}
}
