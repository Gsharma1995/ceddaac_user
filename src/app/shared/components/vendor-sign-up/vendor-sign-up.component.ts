import { Router } from '@angular/router';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { SharedService } from 'src/app/shared/shared.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-vendor-sign-up',
  templateUrl: './vendor-sign-up.component.html',
  styleUrls: ['./vendor-sign-up.component.css']
})
export class VendorSignUpComponent implements OnInit {
  signupForm: FormGroup;
  signupSubmitted: boolean;
  duplicateEmailDbounce;
  massage: any;
  User_Flag:boolean=false;
  Vendor_Flag:boolean=false;
  constructor(private formBuilder: FormBuilder,
    private sharedService: SharedService, private http: HttpClient,public router:Router) { }

  ngOnInit(): void {
    this.signupForm = this.formBuilder.group({
      company_name: ['', Validators.required],
      poc_name: ['', Validators.required],
      poc_title: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(30)]],
      // poc_number: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')], [this.isEmailUnique.bind(this)]
      // ],
      poc_email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      poc_number: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      password: ['', Validators.required],
    });

  }

  get s() { return this.signupForm.controls; }

  closeSignupmodel() {
    this.router.navigate(['/home'])
    this.signupReset();
    // this.sharedService.setVendorSignup(false);
  }

  signupSubmit() {
    this.signupSubmitted = true;
    if (this.signupForm.valid) {
      this.sharedService.postData("register_vendor", this.signupForm.value).subscribe((response: any) => {
        if(response.replyCode == 'success'){
          this.signupSubmitted = false;
          this.signupForm.reset();
          this.sharedService.showSuccess(response.replyMsg);
          this.router.navigate(['/vendor-login'])
        }else{
          this.sharedService.showError(response.replyMsg);
        }
      });
    }
  };

  signupReset() {
    this.signupSubmitted = false;
    this.signupForm.reset();
  }

  openLoginPopup() {
    this.closeSignupmodel();
    // this.sharedService.setVendorLogin(true);
  }

  isEmailUnique(control: FormControl) {
    clearTimeout(this.duplicateEmailDbounce);
    const q = new Promise((resolve, reject) => {
      this.duplicateEmailDbounce = setTimeout(() => {
        if (control.value && control.value.length > 0) {
          return this.sharedService.postData("users/CheckUserNameEmailID", { "Username": control.value, "Type": "emailid" }).subscribe((response: any) => {
            if (response && response.errorNY == "N")
              resolve({});
            else
              resolve({ 'duplicateEmail': true });
          },
            () => { resolve({ 'duplicateEmail': true }); });
        }
      }, 1000);
    });
    return q;
  }

}
