import { Component, OnInit, Output, EventEmitter, ViewEncapsulation, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SharedService } from 'src/app/shared/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vendor-login',
  templateUrl: './vendor-login.component.html',
  styleUrls: ['./vendor-login.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class VendorLoginComponent implements OnInit {
  @Input() loginModule: string;
  loginForm: FormGroup;
  submitted: boolean;

  constructor(private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private router: Router) {
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  get f() { return this.loginForm.controls; }

  closemodel() {
    this.router.navigate(['/home'])
    this.formReset();
    this.sharedService.setLogin(false);
  }

  loginSubmit() {
    this.submitted = true;
    if (this.loginForm.valid) {
      this.sharedService.post("vendor_login", this.loginForm.value).subscribe((response: any) => {
        if(response.replyCode == 'success'){
          sessionStorage.setItem('user_type','vendor');
          this.sharedService.setLoggedInStatusVendor(true);
          this.sharedService.showSuccess("Logged in successfully");
          this.sharedService.setTokenAndUserDetails(response.data);
          this.closemodel();
          this.sharedService.setLoggedInStatus(true);
          const isDirectoryLoginClicked = this.sharedService.isDirectoryLoginClicked;
          if (isDirectoryLoginClicked && isDirectoryLoginClicked == 'directory') {
            this.router.navigate(['./directory']);
          }
        }else{
          this.sharedService.showError(response.replyMsg);
        }
        // if (response && response.errorNY == "N") {
        //   let userDetails = {
        //     FirstName: response.firstName,
        //     Token: response.token,
        //     Email: response.email,
        //     LastName: response.lastName,
        //     PictureUrl: response.pictureUrl,
        //     UserId: response.userId,
        //     UserType: response.userType,
        //     Username: response.username
        //   };
        //   this.sharedService.setTokenAndUserDetails(userDetails);
        //   this.sharedService.showSuccess("Logged in successfully");
        //   this.sharedService.setLoggedInStatus(true);
        //   this.closemodel();
        //   const isDirectoryLoginClicked = this.sharedService.isDirectoryLoginClicked;
        //   if (isDirectoryLoginClicked && isDirectoryLoginClicked == 'directory') {
        //     this.router.navigate(['./directory']);
        //   }
        // } else if (response && response.errorNY == "Y") {
        //   this.loginForm.get('password').setErrors({ 'invalid': true, 'message': response.message });
        // }
      },err =>{
        this.sharedService.showError(err);
      });
    }
  };

  formReset() {
    this.submitted = false;
    this.loginForm.reset();
  }

  openSignupPopup() {
    this.router.navigate(['/vendor-signup'])
    // this.closemodel();
    // this.sharedService.setSignup(true);
  }

}
