import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { SharedService } from 'src/app/shared/shared.service';
import { HttpClient } from '@angular/common/http';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  signupForm: FormGroup;
  signupSubmitted: boolean;
  duplicateEmailDbounce;
  massage: any;
  User_Flag:boolean=false;
  Vendor_Flag:boolean=false;
  Otp_Flag:boolean=false;
  otpVal:any;
  registered_email:any;
  constructor(private formBuilder: FormBuilder,
    private sharedService: SharedService, private http: HttpClient,public router:Router) { }

  ngOnInit(): void {
    this.signupForm = this.formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(30)]],
      // Email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')], [this.isEmailUnique.bind(this)]
      // ],
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      mobile: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      checkbox: ['', Validators.required],
    });
    this.sharedService.GetOtpInputShowHide().subscribe(data =>{
      this.Otp_Flag= data;
    })
  }

  get s() { return this.signupForm.controls; }

  closeSignupmodel() {
    this.signupReset();
    this.sharedService.setSignup(false);
  }

  signupSubmit() {
    this.signupSubmitted = true;
    if (this.signupForm.valid) {
      var requestData = {
        first_name:this.signupForm.value.first_name,
        last_name:this.signupForm.value.last_name,
        password:this.signupForm.value.password,
        email:this.signupForm.value.email,
        mobile:this.signupForm.value.mobile
      }
      this.sharedService.postData("register", requestData).subscribe((response: any) => {
        if(response.replyCode == 'success'){
          this.signupSubmitted = false;
          // this.signupForm.reset();
          this.sharedService.showSuccess(response.replyMsg);
          this.sharedService.SetOtpInputShowHide(true);
          this.registered_email= response.id;
          // this.closeSignupmodel();
        }else{
          this.sharedService.showError(response.replyMsg);
        }
      });
    }
  };
  onOtpSubmit(){
    if(this.otpVal != undefined){
      // if(this.otpVal.length >= 6){
        this.sharedService.postData("verify_email", {email:this.signupForm.value.email,otp:this.otpVal}).subscribe((response: any) => {
          if(response.replyCode == 'success'){
            this.signupSubmitted = false;
            this.signupForm.reset();
            this.sharedService.showSuccess(response.replyMsg);
            this.sharedService.SetOtpInputShowHide(false);
            this.closeSignupmodel();
            this.otpVal=undefined;
          }else{
            this.sharedService.showError(response.replyMsg);
          }
        });
      // }else{
      //   this.sharedService.showError('Please Enter Valid 6 digit Otp.');
      // }
    }else{
      this.sharedService.showError('Please Enter Valid 6 digit Otp.');}
  }

  signupReset() {
    this.signupSubmitted = false;
    this.signupForm.reset();
  }

  openLoginPopup() {
    this.closeSignupmodel();
    this.sharedService.setLogin(true);
  }

  isEmailUnique(control: FormControl) {
    clearTimeout(this.duplicateEmailDbounce);
    const q = new Promise((resolve, reject) => {
      this.duplicateEmailDbounce = setTimeout(() => {
        if (control.value && control.value.length > 0) {
          return this.sharedService.postData("users/CheckUserNameEmailID", { "Username": control.value, "Type": "emailid" }).subscribe((response: any) => {
            if (response && response.errorNY == "N")
              resolve({});
            else
              resolve({ 'duplicateEmail': true });
          },
            () => { resolve({ 'duplicateEmail': true }); });
        }
      }, 1000);
    });
    return q;
  }
  openCloseSignuppopup() {
    this.router.navigate(['/vendor-signup']);
    this.sharedService.setSignup(false);
    // this.sharedService.setVendorSignup(true);
  }
}
