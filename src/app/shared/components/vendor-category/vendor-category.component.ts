import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { SharedService } from 'src/app/shared/shared.service';
import { HttpClient } from '@angular/common/http';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vendor-category',
  templateUrl: './vendor-category.component.html',
  styleUrls: ['./vendor-category.component.css']
})
export class VendorCategoryComponent implements OnInit {
  CategoryList: any;
  VendorList: any;
  CategoryArray: any = [];
  Json_Data=JSON.parse(sessionStorage.getItem('UserDetails'));
  constructor(private formBuilder: FormBuilder,
    private sharedService: SharedService, private http: HttpClient, public router: Router) { }

  ngOnInit(): void {
    this.getLoadCategoryList();
    this.getVendorCategoryList();
  }
  closeSignupmodel(){
    this.router.navigate(['/home']);
  }
  getVendorCategoryList(){
    this.sharedService.post('vendor_category_list', { 'vendor_id': this.Json_Data.id}).subscribe((res: any) => {
      if (res.replyCode == 'success') {
        if (res.data.length > 0) {
          this.VendorList = res.data;
          var roleDataProductPayload = this.CategoryList;
          var LoadDataProductPayload = res.data;
          this.CategoryList.forEach(element => {
            res.data.forEach(elements => {
              if(element.id === elements.category_id){
                var obj = element;
                Object.assign(obj, {selectflag: true});
              }
            });
          });
        }
      }
    }, error => {
    });
  }
  getLoadCategoryList() {
    this.sharedService.post('category_drop_down_list', { 'parent_id': '0' }).subscribe((res: any) => {
      if (res.replyCode == 'success') {
        if (res.data.length > 0) {
          this.CategoryList = res.data;
        }
      }
    }, error => {
    });
  }
  addcategory(CategoryObject, value, type) {
    if (type == 'Category') {
      if (value == true) {
        this.CategoryArray.push({ id: CategoryObject.id });
      } else {
        const index: number = this.CategoryArray.indexOf(value);
        this.CategoryArray.splice(index, 1);
      }
    }
  }
  onSubmitCategoryData(){
    var obj={
      "vendor_id":this.Json_Data.id,
      'category':this.CategoryArray
    }
    this.sharedService.post('add_vendor_category', obj).subscribe((res: any) => {
      if (res.replyCode == 'success') {
        this.sharedService.showSuccess(res.replyMsg);
      }
    }, error => {
    });
  }
}
