import { Component, OnInit, Output, EventEmitter, ViewEncapsulation, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SharedService } from 'src/app/shared/shared.service';
import { Router } from '@angular/router';

import { SocialAuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { SocialUser } from "angularx-social-login";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
  @Input() loginModule: string;
  loginForm: FormGroup;
  submitted: boolean;
  user: SocialUser;
  loggedIn: boolean;
  constructor(private formBuilder: FormBuilder,
    private sharedService: SharedService,private authService: SocialAuthService,
    private router: Router) {
  }
  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(x => 
      console.log('x::::',x)
      );

  }
  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
    this.authService.authState.subscribe((user) => {
      // id === user.id,user.email,user.authToken
      if(user){
        this.user = user;
        this.loggedIn = (user != null);
        var requestData = {
          first_name:this.user.firstName,
          last_name:this.user.lastName,
          password:'123456',
          email:this.user.email,
          mobile:'',
          social_media:this.user.provider,
          social_id:this.user.id
        }
        this.sharedService.postData("register", requestData).subscribe((response: any) => {
          if(response.replyCode == 'success'){
            this.sharedService.setLogin(false);
            this.sharedService.setLoggedInStatus(true);
            sessionStorage.setItem('user_type','user');
            setTimeout(() => {
              this.sharedService.showSuccess("Logged in successfully");
            }, 1000);
            this.sharedService.setTokenAndUserDetails(response.data);
            const isDirectoryLoginClicked = this.sharedService.isDirectoryLoginClicked;
            if (isDirectoryLoginClicked && isDirectoryLoginClicked == 'directory') {
              this.router.navigate(['./directory']);
            }
          }else{
            this.sharedService.showError(response.replyMsg);
          }
        });
      }
    });
  }

  get f() { return this.loginForm.controls; }
  onForgotPassword(){
    this.sharedService.setLogin(false);
    this.sharedService.SetisForgotOpen(true);
  }
  closemodel() {
    this.formReset();
    this.sharedService.setLogin(false);
  }

  loginSubmit() {
    this.submitted = true;
    if (this.loginForm.valid) {
      this.sharedService.post("login", this.loginForm.value).subscribe((response: any) => {
        if(response.replyCode == 'success'){
          sessionStorage.setItem('user_type','user');
          this.sharedService.showSuccess("Logged in successfully");
          this.sharedService.setTokenAndUserDetails(response.data);
          this.closemodel();
          this.sharedService.setLoggedInStatus(true);
          const isDirectoryLoginClicked = this.sharedService.isDirectoryLoginClicked;
          if (isDirectoryLoginClicked && isDirectoryLoginClicked == 'directory') {
            this.router.navigate(['./directory']);
          }
        }else{
          this.sharedService.showError(response.replyMsg);
        }
        // if (response && response.errorNY == "N") {
        //   let userDetails = {
        //     FirstName: response.firstName,
        //     Token: response.token,
        //     Email: response.email,
        //     LastName: response.lastName,
        //     PictureUrl: response.pictureUrl,
        //     UserId: response.userId,
        //     UserType: response.userType,
        //     Username: response.username
        //   };
        //   this.sharedService.setTokenAndUserDetails(userDetails);
        //   this.sharedService.showSuccess("Logged in successfully");
        //   this.sharedService.setLoggedInStatus(true);
        //   this.closemodel();
        //   const isDirectoryLoginClicked = this.sharedService.isDirectoryLoginClicked;
        //   if (isDirectoryLoginClicked && isDirectoryLoginClicked == 'directory') {
        //     this.router.navigate(['./directory']);
        //   }
        // } else if (response && response.errorNY == "Y") {
        //   this.loginForm.get('password').setErrors({ 'invalid': true, 'message': response.message });
        // }
      },err =>{
        this.sharedService.showError(err);
      });
    }
  };

  formReset() {
    this.submitted = false;
    this.loginForm.reset();
  }

  openSignupPopup() {
    this.closemodel();
    this.sharedService.setSignup(true);
  }

  openSignupVendorPopup() {
    this.closemodel();
    this.sharedService.setSignup(false);
    this.router.navigate(['/vendor-login'])
  }
}
