import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SharedService } from '../shared.service';
import { NgxSpinnerService } from "ngx-spinner"; 

@Injectable()

export class HttpConfigInterceptor implements HttpInterceptor {
    constructor(private router: Router, private sharedService: SharedService,
        private spinnerService: NgxSpinnerService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.spinnerService.show();
        request = request.clone({
            setHeaders: {
                // Authorization: `Bearer ${this.sharedService.getToken()}`,
                // 'Content-Type': 'multipart/form-data'
            }
        });
        return next.handle(request).pipe(
            catchError(
                (err, caught) => {
                    if (err.status === 401) {
                        this.handleAuthError();
                        return of(err);
                    }
                    throw err;
                }
            )
            ,finalize(() =>  this.spinnerService.hide())
        );
    }

    private handleAuthError() {
        this.sharedService.showError('Please login to proceed');
        this.sharedService.signOut();
    }
}