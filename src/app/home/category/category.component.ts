import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  changeText1:boolean=false;
  changeText2:boolean=false;
  changeText:boolean=false;
  userDetails: any;
  // @Output() closeLogipopup = new EventEmitter<boolean>();

  constructor(private router: Router, private sharedService: SharedService) { }

  ngOnInit(): void {
    // this.loadCalculatorObject();
    this.sharedService.GetRouteType().subscribe(route =>{
      // alert();
      this.onScrollElement(route)
    })
  }
  onScrollElement(type){
    const itemToScrollTo = document.getElementById(type);
    if (itemToScrollTo) {
      itemToScrollTo.scrollIntoView(true);
    }
  }
  loadDirectory() {
    this.sharedService.SetPortFolio_PopUp(false);
    this.sharedService.SetBanner_Tempalte(false);
    this.sharedService.SetVendor_Banner_Tempalte(true);
    this.router.navigate(['./directory']);
    // this.userDetails = this.sharedService.getUserDetails();
    // if (this.userDetails && this.userDetails.length > 0) {
    //   this.router.navigate(['./directory']);
    //   this.sharedService.SetPortFolio_PopUp(false);
    // }
    // else {
    //   this.sharedService.isDirectoryLoginClicked = "directory";
    //   this.sharedService.setLogin(true);
    // }
  }

  loadCalculatorObject() {
    var data = {
      Typology: "",
      LocationCoordinates: "",
      LocationText: "",
      Area: "",
      Unit: "",
      Areainsqft: "",
      SelectedAreainsqft: "",
      TOPOGRAPHY: "",
      Length1: "",
      Length2: "",
      Length3: "",
      Length4: "",
      TypeOfProject: "",
      TypeOfProjectID: "",
      TypeOfProjectLevel1Selected: "",
      TypeOfProjectLevel1ID: "",
      TypeOfProjectLevel2Selected: "",
      TypeOfProjectLevel2ID: "",
      TypeOfProjectLevel3Selected: "",
      TypeOfProjectLevel3ID: "",
      Class: "",
      city_id: "",
      selected_city_id:""
    }
    this.sharedService.SetHomeCategoryComponentFlag(false);
    this.sharedService.setCalculatorData(data);
  }

}
