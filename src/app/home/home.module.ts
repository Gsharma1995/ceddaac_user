import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { CategoryComponent } from './category/category.component';
import { TypologyComponent } from '../calculator/typology/typology.component';
import { TypologyHomeComponent } from '../calculator/typology-home/typology-home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TopographyHomeComponent } from '../calculator/topography-home/topography-home.component';
import { TopographyModelComponent } from '../calculator/topography-model/topography-model.component';
import { TopographyComponent } from '../calculator/topography/topography.component';
import { CostplanpopupComponent } from '../calculator/costplanpopup/costplanpopup.component';
import { BHKPopupComponent } from '../calculator/bhk-popup/bhk-popup.component';
import { WorkspaceComponent } from '../calculator/workspace/workspace.component';
import { Level1Component } from '../calculator/level1/level1.component';
import { Level2Component } from '../calculator/level2/level2.component';
import { Level3Component } from '../calculator/level3/level3.component';
import { WorkspacePopupComponent } from '../calculator/workspace-popup/workspace-popup.component';
import { BreadcrumbComponent } from '../component/breadcrumb/breadcrumb.component';
import { AgmCoreModule, LAZY_MAPS_API_CONFIG, LazyMapsAPILoaderConfigLiteral } from '@agm/core';
import { WebsiteComponent } from './website/website.component';
import { TermsandconditionComponent } from './termsandcondition/termsandcondition.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { CookiepolicyComponent } from './cookiepolicy/cookiepolicy.component';
import { LayoutComponent } from '../calculator/layout/layout.component';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { MapBoxComponent } from '../component/map-box/map-box.component';

const BASE_MODULES = [
  CommonModule,
  HomeRoutingModule,
  FormsModule,
  ReactiveFormsModule,AutocompleteLibModule
]
const COMPONENTS = [
  HomeComponent,
  CategoryComponent,
  TypologyComponent,
  TypologyHomeComponent,
  TopographyHomeComponent,
  TopographyModelComponent,
  TopographyComponent,
  CostplanpopupComponent,
  BHKPopupComponent,
  WorkspaceComponent,
  Level1Component,
  Level2Component,
  Level3Component,
  WorkspacePopupComponent,
  WebsiteComponent,
  TermsandconditionComponent,
  PrivacypolicyComponent,
  CookiepolicyComponent,LayoutComponent,MapBoxComponent
];
const COMMON_COMPONENTS = [
  BreadcrumbComponent
];
@NgModule({
  declarations: [...COMPONENTS,...COMMON_COMPONENTS],
  imports: [
    ...BASE_MODULES,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyArmUOJh4WJyBe-VI5JNL7bc4BOThIzhhY",
      libraries: ["places"]
    }),
  ]
})
export class HomeModule { }
