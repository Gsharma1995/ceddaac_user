import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { CategoryComponent } from './category/category.component';
import { TypologyHomeComponent } from '../calculator/typology-home/typology-home.component';
import { TypologyComponent } from '../calculator/typology/typology.component';
import { TopographyHomeComponent } from '../calculator/topography-home/topography-home.component';
import { TopographyComponent } from '../calculator/topography/topography.component';
import { WorkspaceComponent } from '../calculator/workspace/workspace.component';
import { Level1Component } from '../calculator/level1/level1.component';
import { Level2Component } from '../calculator/level2/level2.component';
import { Level3Component } from '../calculator/level3/level3.component';
import { TermsandconditionComponent } from './termsandcondition/termsandcondition.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { CookiepolicyComponent } from './cookiepolicy/cookiepolicy.component';
import { LayoutComponent } from '../calculator/layout/layout.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent, 
    children: [
      {
        path: '', 
        component: CategoryComponent,
      },
      {
        path: 'typology', 
        component: TypologyHomeComponent,
      },
      {
        path: 'typology/:id', 
        component: TypologyComponent,
       
      },
      {
        path: 'topography-home', 
        component: TopographyHomeComponent,
       
      },
      {
        path: 'topography', 
        component: TopographyComponent,
       
      },
     
      {
        path: 'level1', 
        component: Level1Component,
       
      },
      {
        path: 'level2', 
        component: Level2Component,
       
      },
      {
        path: 'level3', 
        component: Level3Component,
       
      },{
        path: 'layouts', 
        component: LayoutComponent,
       
      },
      {
        path: 'workspace', 
        component: WorkspaceComponent,
       
      },
      {
        path: 'termsandcondition', 
        component: TermsandconditionComponent,
       
      },
      {
        path: 'privacypolicy', 
        component: PrivacypolicyComponent,
       
      },
      {
        path: 'cookiepolicy', 
        component: CookiepolicyComponent,
       
      },
     
    ],
  }
];

@NgModule({
  
  imports: [RouterModule.forChild(routes)
],

  exports: [RouterModule]
})
export class HomeRoutingModule {
}
