import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
// import { $ } from 'protractor';
declare var $ :any;

@Component({
  selector: 'app-website',
  templateUrl: './website.component.html',
  styleUrls: ['./website.component.scss']
})
export class WebsiteComponent implements OnInit {
  isLoggedIn: boolean;
  userName: string;
  editProfile: boolean;
  Vendor_flag:boolean=false;
  @ViewChild('seachKey', { static: false }) seachKey: ElementRef;
  tab1:boolean=true;
  tab2:boolean=false;
  tab3:boolean=false;
  tab4:boolean=false;
  tab5:boolean=false;
  tab6:boolean=false;
  careers1:boolean=true;
  careers2:boolean=false;
  careers3:boolean=false;
  careers4:boolean=false;
  careers5:boolean=false;
  careers6:boolean=false;
  Addform: FormGroup;
  public submitted = false;public btnsubmitted = false;
  Blog_List:any=[];
  User_Data=JSON.parse(sessionStorage.getItem('UserDetails'));
  constructor(private fb: FormBuilder,private router: Router,
    public sharedService: SharedService,
    private http: HttpClient, 
    public sanitizer: DomSanitizer) {
      this.GetBlogList();
  }
  allowurl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  getCategorydata(type, CategoryObject) {
    // this.Category_Type=type;
    this.DirectoryCategoryMenu.forEach(element => {
      element.flag = false;
    });
    CategoryObject.flag = true;
    // this.GetVendorList('',this.Category_Type);
    // this.valueChange.emit(false);
    this.sharedService.SetPortFolio_PopUp(false);
  }
  onSubmitReview(){
    console.log(this.Addform.value)
    this.submitted =true;
    if (this.Addform.invalid) {
      return;
    } else {
      this.btnsubmitted = true;
      var requestData = this.Addform.value;
      // if(JSON.parse(sessionStorage.getItem('vendorId'))){
      //   requestData.vendor_id =JSON.parse(sessionStorage.getItem('vendorId')); 
      // }else{
      //   requestData.vendor_id =''; 
      // }
      if(this.User_Data){
        requestData.user_id =this.User_Data.id; 
      }else{
        requestData.user_id =''; 
      } 
      var description = `My name is ${this.Addform.value.name}, and I work with ${this.Addform.value.work},I am a/an ${this.Addform.value.provider} from ${this.Addform.value.from}, I ${this.Addform.value.want}, to talk to you about ${this.Addform.value.about} Would you mail me at ${this.Addform.value.mail} ? And, ${this.Addform.value.signin}`;
      requestData.title ='';
      requestData.description =description;
      requestData.vendor_id =''; 
      requestData.type ='0'; 
      this.sharedService.post('send_feedback', requestData).subscribe((resp: any) => {
        if(resp.replyCode == "success"){
          this.submitted=false;
          this.btnsubmitted=false;
          this.sharedService.showSuccess(resp.replyMsg);
        }else{
          this.sharedService.showError(resp.replyMsg);
        }
        this.Addform.reset();
        this.sharedService.SetReachOutPopUp(false);
        this.submitted=false;
        this.btnsubmitted=false;
        // this.sharedService.showError("Oops! Something went wrong!");
      });
    }
  }
  DirectoryCategoryMenu: any;
  index: number = 1;
  onSectionRedirect(type){
    this.sharedService.SetRouteType(type);
  }
  onScrollElement(type){
    const itemToScrollTo = document.getElementById(type);
    if (itemToScrollTo) {
      itemToScrollTo.scrollIntoView(true);
    }
  }
  ngOnInit(): void {
    $('#news_image2').addClass('z_index');
    this.sharedService.GetRouteType().subscribe(route =>{
      // alert();
      this.onScrollElement(route)
    })
    this.Addform = this.fb.group({
      name: ['', Validators.required],
      work: ['', Validators.required],
      provider: ['', Validators.required],
      from: ['', Validators.required],
      want: ['', Validators.required],
      about: ['', Validators.required],
      mail: ['', Validators.required],
      signin: ['', Validators.required]
    });
    this.DirectoryCategoryMenu = [
      { name: 'All', link: '', index: 0, flag: false },
      { name: 'Design', link: 'Design', index: 1, flag: false },
      { name: 'Art', link: 'Art', index: 2, flag: false },
      { name: 'Architecture', link: 'Architecture', index: 3, flag: false },
      { name: 'Construction', link: 'Construction', index: 4, flag: false },
    ]
    this.DirectoryCategoryMenu.forEach(element => {
      if (element.index == this.index) {
        element.flag = true;
      }
    });
    var lastScrollTop = 0;
    $(window).scroll(function(event){
       var st = $(this).scrollTop();
       if (st > lastScrollTop){
           // downscroll code
       } else {
          // upscroll code
       }
       lastScrollTop = st;
    });
  }
  @HostListener('window:scroll', ['$event']) // for window scroll events
  onScroll(event){
    // this.scrollObject = event;
  }
  scroll(id) {
    // this.sharedService.getLoggedInStatusVendor().subscribe(data =>{
    //   if(data == true){
    //     this.Vendor_flag=true;
    //   }else{
    //     this.Vendor_flag=false;
    //     if(!(sessionStorage.getItem('UserDetails'))){
    //       // alert('PopUp')
    //       this.sharedService.isDirectoryLoginClicked = "";
    //       this.sharedService.setLogin(true);
    //     }else{
          
    //     }
    //   }
    // })
    
    // if(event.timeStamp > 8000){
    //   alert('PopUp')
    // }
    // let el = document.getElementById(id);
    // el.scrollIntoView();
  }
  onActiveClass(type:string) {
    if(type == 'tab1'){
      this.tab1=true;
      this.tab2=false;
      this.tab3=false;
      this.tab4=false;
      this.tab5=false;
      this.tab6=false;
    }else if(type == 'tab2'){
      this.tab1=false;
      this.tab2=true;
      this.tab3=false;
      this.tab4=false;
      this.tab5=false;
      this.tab6=false;
    }else if(type == 'tab3'){
      this.tab1=false;
      this.tab2=false;
      this.tab3=true;
      this.tab4=false;
      this.tab5=false;
      this.tab6=false;
    }else if(type == 'tab4'){
      this.tab1=false;
      this.tab2=false;
      this.tab3=false;
      this.tab4=true;
      this.tab5=false;
      this.tab6=false;
    }else if(type == 'tab5'){
      this.tab1=false;
      this.tab2=false;
      this.tab3=false;
      this.tab4=false;
      this.tab5=true;
      this.tab6=false;
    }else if(type == 'tab6'){
      this.tab1=false;
      this.tab2=false;
      this.tab3=false;
      this.tab4=false;
      this.tab5=false;
      this.tab6=true;
    }
  }

  onActiveClassCareers(type:string) {
    if(type == 'careers1'){
      this.careers1=true;
      this.careers2=false;
      this.careers3=false;
      this.careers4=false;
      this.careers5=false;
      this.careers6=false;
    }else if(type == 'careers2'){
      this.careers1=false;
      this.careers2=true;
      this.careers3=false;
      this.careers4=false;
      this.careers5=false;
      this.careers6=false;
    }else if(type == 'careers3'){
      this.careers1=false;
      this.careers2=false;
      this.careers3=true;
      this.careers4=false;
      this.careers5=false;
      this.careers6=false;
    }else if(type == 'careers4'){
      this.careers1=false;
      this.careers2=false;
      this.careers3=false;
      this.careers4=true;
      this.careers5=false;
      this.careers6=false;
    }else if(type == 'careers5'){
      this.careers1=false;
      this.careers2=false;
      this.careers3=false;
      this.careers4=false;
      this.careers5=true;
      this.careers6=false;
    }else if(type == 'careers6'){
      this.careers1=false;
      this.careers2=false;
      this.careers3=false;
      this.careers4=false;
      this.careers5=false;
      this.careers6=true;
    }
  }
  onImageActive(type){
    // alert();
      $('#news_image1').removeClass('z_index');
      $('#news_image2').removeClass('z_index');
      $('#news_image3').removeClass('z_index');
    if(type == 'image1'){
      $('#news_image1').addClass('z_index');
    }else if(type == 'image2'){
      $('#news_image2').addClass('z_index');
    }else if(type == 'image3'){
      $('#news_image3').addClass('z_index');
    }
    
  }
  GetBlogList() {
    this.http.get('https://ceddaac.com/blog/wp-json/wp/v2/posts?_embed').subscribe((response: any) => {
      this.Blog_List=response;
    });
  }
  onRedirect(link){
    // window.location.href = link;
    // target = "_blank";
    var a = document.createElement('a');
        a.target="_blank";
        a.href=link;
        a.click();
  }
}
