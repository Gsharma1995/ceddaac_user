import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
declare var $:any;
import { SocialAuthService } from "angularx-social-login";
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  isLoggedIn: boolean;
  userName: string;
  editProfile: boolean;
  Vendor_flag:boolean=false;
  Home_Link_flag:boolean=true;
  @ViewChild('seachKey', { static: false }) seachKey: ElementRef;
  constructor(private router: Router,
    private sharedService: SharedService,private authService: SocialAuthService,
    private http: HttpClient) {
      // this.sharedService.SetHomeCategoryComponentFlag(true);
  }
  VendorParentCategoryListData: any = [];
  Category_Type:any='Design';
  Menu_Link:any=[
    {name:'How it works',value:'works',flag:true},
    {name:'about',value:'aboutUs',flag:false},
    {name:'blog',value:'blog_section',flag:false},
    // {name:'news',value:'news',flag:false},
    // {name:'careers',value:'careers',flag:false},
    {name:'contact',value:'contact',flag:false},
  ]
  onRedirectTemplate(event){
    this.Menu_Link.forEach(element => {
      element.flag = false;
    });
    event.flag = true;
    this.sharedService.SetRouteType(event.value)
  }
  MyFunction() {
    //add logic here to conditionally return true or false
    // false means no reloading.
    return false;
  }
  ngOnInit(): void {
    $(function(){
      $("a.nav-link").click(function(e)
      {
        // this.Menu_Link.forEach(element => {
        //   element.flag = false;
        // });
        // event.flag = true;
        // var Navlink= e.target.hash.replace(/^#/, '');
        // this.sharedService.SetRouteType(Navlink);
        return false; // prevent default browser refresh on "#" link
      });
  });
    
  // alert();
  // sessionStorage.removeItem('add-topography');
  this.sharedService.GetHomeCategoryComponentFlag().subscribe(data =>{
    if(data == true){
      this.Home_Link_flag=true;
    }else{
      this.Home_Link_flag=false;
    }
  })
    this.sharedService.getLoggedInStatusVendor().subscribe(data =>{
      if(data == true){
        this.Vendor_flag=true;
      }else{
        this.Vendor_flag=false;
      }
    })
    this.sharedService.GetActiveMenuLink().subscribe(data =>{
      this.Category_Type=data;
    })
    let userDetails = JSON.parse(this.sharedService.getUserDetails());
    if(sessionStorage.getItem('user_type')){
      if(sessionStorage.getItem('user_type') == 'vendor'){
        this.sharedService.setLoggedInStatusVendor(true);
        this.userName= userDetails.poc_name;
      }else{
        this.sharedService.setLoggedInStatusVendor(false);}
    }
    this.sharedService.destoryCalculatorRouter();
    this.loadCalculatorRouter();
    this.sharedService.getLoggedInStatus().subscribe(value => {
      let userDetails = this.sharedService.getUserDetails();
      if (userDetails && userDetails.length > 0) {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = value;
      }
    });
  }
  onSubscibe(){
    // alert();
    $('#subscribe').modal('show');
    this.sharedService.SetisSubscribeOpen(true)
  }
  onClickLogin() {
    this.sharedService.isDirectoryLoginClicked = "";
    this.sharedService.setLogin(true);
  }

  onClickDirectory(value) {
    this.sharedService.isDirectoryLoginClicked = "directory";
    this.sharedService.setLogin(true);
  }

  openCloseSignuppopup() {
    this.sharedService.setSignup(true);
  }

  openEditProfile() {
    this.sharedService.setEditProfile(true);
  }

  loadHome(event) {
    this.sharedService.SetHomeCategoryComponentFlag(true);
    this.sharedService.SetRouteType(event);
    this.router.navigate(['./home']);
  }

  signOut() {
    this.sharedService.setLoggedInStatusVendor(false);
    this.sharedService.signOut();
    this.sharedService.SetBanner_Tempalte(false);
    this.sharedService.SetSearch_Input('');
    this.sharedService.SetPortFolio_PopUp(false);
    sessionStorage.removeItem('user_type');
    this.authService.signOut();
  }
  getUserDetails() {
    let currentUser = this.sharedService.getUserDetails()
    if (currentUser && currentUser.length > 0) {
      var user = JSON.parse(currentUser);
      // this.userName = user.FirstName + ' ' + user.LastName;
    }
  }
  loadCalculatorRouter(){
    var data = {
      routerLink1: "home",
      routerLink2: "",
      routerLink3: "",
      routerLink4: "",
      routerLink5: "",
      routerLink6: "",
      routerLink7: "",
      routerLink8: "",
      routerLink9: "",
      routerLink10: "",
      routerLink11: "",
      routerLink12: "",
      routerLink13: "",
    }
    this.sharedService.setCalculatorRouter(data);
  }
  onClickPortFolio(){
    // $('#port').modal('show');
    // this.sharedService.SetVendorPortFolioPopUp(true);
    this.sharedService.SetVendorPortFolioPopUpList(true);
  }
  onSearch(){
    // if(this.seachKey.nativeElement.value)
    this.router.navigateByUrl('/directory');
    this.sharedService.SetBanner_Tempalte(false);
    this.sharedService.SetSearch_Input(this.seachKey.nativeElement.value);
    this.GetVendorList(this.seachKey.nativeElement.value,this.Category_Type);
  }
  GetVendorList(keyword,type) {
    this.sharedService.SetActiveMenuLink(type);
    var RequestData = {
      'keyword': keyword,
      'type': type
    }
    this.sharedService.post('web_vendor_category_list', RequestData).subscribe((response: any) => {
      this.VendorParentCategoryListData = response.data;
      this.sharedService.SetVendorCategoryData(this.VendorParentCategoryListData);
    });
  }
}
