import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { SharedService } from './shared/shared.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'CEDdaac';
  openEditProfile = false;
  openLogin = false;
  openSignup = false;
  openForgot = false;
  openVendor = false;
  openVendorList = false;
  openSubscribeModal = false;
  openChangePasswordModal = false;
  openReachOutModal = false;
  constructor(private sharedService: SharedService) {

  }

  ngOnInit() {
    this.sharedService.GetReachOutPopUp().subscribe(response => {
      this.openReachOutModal = response;
    });
    this.sharedService.GetChangePasswordOpen().subscribe(response => {
      this.openChangePasswordModal = response;
    });
    this.sharedService.GetisSubscribeOpen().subscribe(response => {
      this.openSubscribeModal = response;
    });
    this.sharedService.getLogin().subscribe(response => {
      this.openLogin = response;
    });
    this.sharedService.getSignup().subscribe(response => {
      this.openSignup = response;
    });
    this.sharedService.GetisForgotOpen().subscribe(response => {
      this.openForgot = response;
    });
    this.sharedService.getEditProfile().subscribe(response => {
      this.openEditProfile = response;
    });
    this.sharedService.GetVendorPortFolioPopUp().subscribe(response => {
      this.openVendor = response;
    });
    this.sharedService.GetVendorPortFolioPopUpList().subscribe(response => {
      this.openVendorList = response;
    });
  }


  





}
