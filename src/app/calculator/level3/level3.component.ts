import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-level3',
  templateUrl: './level3.component.html',
  styleUrls: ['./level3.component.css']
})
export class Level3Component implements OnInit {
  typeOfProject: any;
  calculatorData: any;
  topographyData: any;
  openCostPlanPopUp: boolean;
  selectedLevel1: number;
  typologyDatails: any;
  typeOfProjectName: string;
  level2SelectedName: string;
  openLevel2PopUp: boolean;
  selectedBhk: string;

  // Flag
  PopUpModalFlag:boolean=false;
  msg:any;
  Record_Flag:boolean=false;
  constructor(public sharedService: SharedService,
    private actRoute: ActivatedRoute,
    private router: Router, public sanitizer: DomSanitizer) { }
    ngAfterViewInit() {
      setTimeout(() => {
        this.sharedService.SetHomeCategoryComponentFlag(false);
      }, 200);
    }
  ngOnInit(): void {
    this.sharedService.destoryBhkDetails();
    this.topographyData = JSON.parse(this.sharedService.getTopographyData());
    this.typologyDatails = JSON.parse(this.sharedService.getTopologyData());
    this.getCalculatorData();
  }
  allowurl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  getCalculatorData() {
    const apiCalculatorData = this.sharedService.getCalculatorData() as any;
    this.calculatorData = JSON.parse(apiCalculatorData);
    const getData = JSON.parse(this.sharedService.getTopologyData());
    var Object={
      "parent_id":this.calculatorData.TypeOfProjectLevel2ID,
      "city_id":this.calculatorData.city_id,
      'topology_id':getData.id
    }
    this.sharedService.post("web_type_of_project_list", Object).subscribe((response: any) => {
      if (response.replyCode == 'success') {
        if(response.data.length > 0){
          this.typeOfProject = response.data;
        }else{
          this.Record_Flag = true;
          this.msg = 'No layout found of this project type......'
        }
      }
    }, err => {
    });
    // if (apiCalculatorData && apiCalculatorData.length > 0 && calculatorDataToSave && calculatorDataToSave.length > 0) {
    //   const apiData = JSON.parse(apiCalculatorData);
    //   this.calculatorData = JSON.parse(calculatorDataToSave);
    //   if (this.calculatorData && apiData && apiData.data && apiData.data.length > 0 && apiData.layouts && apiData.layouts.length > 0) {
    //     const data = apiData.data.filter(x => x.inputId == this.calculatorData.TOPOGRAPHY);
    //     const typologyData = apiData.data.filter(x => x.inputId == this.calculatorData.Typology);
    //     const level2ID = apiData.data.filter(x => x.inputId == this.calculatorData.TypeOfProject);
    //     this.typeOfProject = apiData.layouts;
    //     this.level2SelectedName = this.calculatorData.TypeOfProjectLevel1Selected;

    //     this.selectedBhk = this.calculatorData.TypeOfProjectLevel2Selected;

    //     if (level2ID && level2ID.length > 0) {
    //       this.typeOfProjectName = level2ID[0].inputName;
    //     }
    //     if (data && data.length > 0) {
    //       this.topographyData = data[0];
    //     }
    //     if (typologyData && typologyData.length > 0) {
    //       this.typologyDatails = typologyData[0];
    //     }
    //   }
    // }
  }

  navigateToTopography() {
    this.router.navigate(['home/topography-home']);
  }

  saveTypeofProjectDetails(typeOfProjects, typeOfProjectInputName,ProjectObject) {
    const getData = JSON.parse(this.sharedService.getTopographyData());
    const apiCalculatorData = this.sharedService.getCalculatorData() as any;
    this.calculatorData = JSON.parse(apiCalculatorData);
    this.calculatorData.TypeOfProjectLevel3ID = typeOfProjects;
    this.calculatorData.TypeOfProjectLevel3Selected = typeOfProjectInputName;
    this.calculatorData.TypeOfProjectID = typeOfProjects;
    this.sharedService.setCalculatorData(this.calculatorData);
    // typography_id, layout_id , area
    this.router.navigate(['home/layouts']);
    return
    var request_data={
      "typography_id":getData.typography_id,
      "type_of_project_id":typeOfProjects,
      "area":this.calculatorData.SelectedAreainsqft
    }
    this.sharedService.post('web_layout_list',request_data).subscribe((data:any)=>{
      if(data.data.length > 0){
        this.sharedService.setBhkDetails(data.data);
        // this.openLevel2PopUp = true;
        // this.router.navigate(['home/layouts']);
      }
      this.calculatorData.TypeOfProjectLevel3ID = typeOfProjects;
      this.calculatorData.TypeOfProjectLevel3Selected = typeOfProjectInputName;
      this.sharedService.setCalculatorData(this.calculatorData);
      // this.sharedService.setBhkDetails(ProjectObject);
      // this.openLevel2PopUp = true;
      if(ProjectObject){
        this.router.navigate(['/home/layouts']);
      }
    })
  }

  openCloseCostPlanModel(openClosePopUp) {
    var JsonBhkDetailsData:any= JSON.parse(this.sharedService.getBhkDetails());
    if(this.selectedBhk == undefined || this.selectedBhk == null || this.selectedBhk == ''){
      this.PopUpModalFlag = true;
    }else{
      this.PopUpModalFlag = false;
      this.openCostPlanPopUp = openClosePopUp;
    }
    
    // this.openCostPlanPopUp = openClosePopUp;
  }

  openCloseLevel2Model(openClosePopUp) {
    this.openLevel2PopUp = openClosePopUp;
  }

  setBhkName(bhkName) {
    if(bhkName == undefined || bhkName == null || bhkName == ''){
      this.PopUpModalFlag = true;
    }else{
      this.selectedBhk = bhkName;
      this.PopUpModalFlag = false;
    }
  }
}
