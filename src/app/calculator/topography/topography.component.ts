import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-topography',
  templateUrl: './topography.component.html',
  styleUrls: ['./topography.component.css']
})
export class TopographyComponent implements OnInit {
  // typeOfProject: any;
  // topography_Id = 0;
  // topographyDetails: any;
  // calculatorData: any;
  // topographyData: any;
  // openCostPlanPopUp: boolean;
  // level1Flag = true;
  // level2Flag = true;
  // openLevel2PopUp: boolean;
  // selectedBhk: string;
  // selectedLevel1: number;
  // selectedLevel2: number;
  // selectedLevel3: number;
  // getCalculatorDatails: any;
  // topographyDatails: any;
  // typologyDatails: any;
  // TypeOfProject: any;
  // TypeOfProjectLevel1ID: any;

  constructor(private sharedService: SharedService,
    private actRoute: ActivatedRoute,
    private router: Router) { }
    ngAfterViewInit() {
      setTimeout(() => {
        this.sharedService.SetHomeCategoryComponentFlag(false);
      }, 200);
    }
  ngOnInit(): void {
    // this.getCalculatorData();
    // const getData = this.sharedService.getCalculatorData();
    // if (getData && getData.length > 0) {
    //   debugger
    //   this.getCalculatorDatails = JSON.parse(getData);
    //   if (this.getCalculatorDatails && this.getCalculatorDatails.TypeOfProject) {
    //     this.selectedLevel1 = this.getCalculatorDatails.TypeOfProject;
    //   }
    //   if (this.getCalculatorDatails && this.getCalculatorDatails.TypeOfProjectLevel1ID) {
    //     this.selectedLevel2 = this.getCalculatorDatails.TypeOfProjectLevel1ID;
    //   }
    //   if (this.getCalculatorDatails && this.getCalculatorDatails.TypeOfProjectLevel2Selected) {
    //     this.selectedBhk = this.getCalculatorDatails.TypeOfProjectLevel2Selected;
    //   }
    }

  //   const apiCalculatorData = this.sharedService.getApiCalculatorData() as any;
  //   if (apiCalculatorData && apiCalculatorData.length > 0) {
  //     const apiData = JSON.parse(apiCalculatorData);
  //     if (apiData && apiData.data && apiData.data.length > 0) {
  //       debugger
  //       var data = apiData.data.filter(x => x.inputId == this.getCalculatorDatails.TOPOGRAPHY);
  //       var details = apiData.data.filter(x => x.inputId == this.getCalculatorDatails.Typology);
  //       var detail = apiData.data.filter(x => x.inputId == this.getCalculatorDatails.TypeOfProject);
  //       var deta = apiData.data.filter(x => x.inputId == this.getCalculatorDatails.TypeOfProjectLevel1ID);

  //       if (data && data.length > 0 && details && details.length > 0) {
  //         this.topographyDatails = data[0];
  //         this.typologyDatails = details[0];
  //       }
  //       if (detail && detail.length > 0) {
  //         this.TypeOfProject = detail[0];
  //       }
  //       if (deta && deta.length > 0) {
  //         this.TypeOfProjectLevel1ID = deta[0];
  //       }
  //     }
  //   }
  // }


  // getCalculatorData() {
  //   const apiCalculatorData = this.sharedService.getApiCalculatorData() as any;
  //   const calculatorDataToSave = this.sharedService.getCalculatorData();
  //   if (apiCalculatorData && apiCalculatorData.length > 0 && calculatorDataToSave && calculatorDataToSave.length > 0) {
  //     const apiData = JSON.parse(apiCalculatorData);
  //     this.calculatorData = JSON.parse(calculatorDataToSave)
  //     if (apiData && apiData.data && apiData.data.length > 0) {
  //       var data = apiData.data.filter(x => x.inputId == this.calculatorData.TOPOGRAPHY);
  //       this.typeOfProject = apiData.data.filter(x => x.inputType == "TypeOfProject");
  //       if (data && data.length > 0) {
  //         this.topographyData = data[0];
  //       }
  //     }
  //   }
  // }

  // navigateToTopography() {
  //   this.router.navigate(['home/topography-home']);
  // }

  // saveTypeofProjectDetails(typeOfProjects, typeOfProjectInputName) {
  //   debugger
  //   if (this.level1Flag) {
  //     this.calculatorData.TypeOfProject = typeOfProjects;
  //     this.sharedService.setCalculatorData(this.calculatorData);

  //     const apiCalculatorData = this.sharedService.getApiCalculatorData();
  //     if (apiCalculatorData && apiCalculatorData.length > 0) {
  //       const apiData = JSON.parse(apiCalculatorData);
  //       if (apiData && apiData.data && apiData.data.length > 0) {
  //         const filteredData = apiData.data.filter(x => x.inputType == typeOfProjectInputName);
  //         if (filteredData && filteredData.length > 0) {
  //           this.typeOfProject = filteredData;
  //           this.level1Flag = false;
  //         }
  //       }
  //     }
  //   }
  //   else if (this.level2Flag) {
  //     this.calculatorData.TypeOfProjectLevel1ID = typeOfProjects;
  //     this.calculatorData.TypeOfProjectLevel1Selected = typeOfProjectInputName;
  //     this.sharedService.setCalculatorData(this.calculatorData);

  //     const apiCalculatorData = this.sharedService.getApiCalculatorData();
  //     if (apiCalculatorData && apiCalculatorData.length > 0) {
  //       const apiData = JSON.parse(apiCalculatorData);
  //       if (apiData && apiData.data && apiData.data.length > 0) {
  //         const filteredData = apiData.data.filter(x => x.inputType == typeOfProjectInputName);
  //         if (filteredData && filteredData.length > 0) {
  //           this.typeOfProject = filteredData;
  //           this.level2Flag = false;
  //         }

  //       }
  //     }
  //   }

  //   else {
  //     this.calculatorData.TypeOfProjectLevel2ID = typeOfProjects;
  //     this.calculatorData.TypeOfProjectLevel2Selected = typeOfProjectInputName;
  //     this.sharedService.setCalculatorData(this.calculatorData);
  //     this.openLevel2PopUp = true;
  //   }
  // }


  // openCloseCostPlanModel(openClosePopUp) {
  //   this.openCostPlanPopUp = openClosePopUp;
  // }

  // openCloseLevel2Model(openClosePopUp) {
  //   this.openLevel2PopUp = openClosePopUp;
  // }

  // setBhkName(bhkName) {
  //   this.selectedBhk = bhkName;

  // }

  // filterLevel2DAta(typeOfProjectInputName) {
  //   const apiCalculatorData = this.sharedService.getApiCalculatorData();
  //   if (apiCalculatorData && apiCalculatorData.length > 0) {
  //     const apiData = JSON.parse(apiCalculatorData);
  //     if (apiData && apiData.data && apiData.data.length > 0) {
  //       return apiData.data.filter(x => x.inputType == typeOfProjectInputName);


  //     }
  //   }
  // }
}
