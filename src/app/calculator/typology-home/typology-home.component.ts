import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { Meta, Title } from '@angular/platform-browser';
import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/shared/shared.service';
import { MouseEvent } from '@agm/core';
// import { } from '@types/googlemaps';
import { MapsAPILoader } from '@agm/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Location } from '@angular/common';
declare const google: any
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-calculator-home',
  templateUrl: './typology-home.component.html',
  styleUrls: ['./typology-home.component.css'],
})
export class TypologyHomeComponent implements OnInit {
  lat: number = 20.5937;
  lng: number = 78.9629;
  typologyData: any;
  selectedTypology: number;
  title = 'My first AGM project';
  @ViewChild("search")
  public searchElementRef: ElementRef;

  subindustries: any;
  industries: any;

  selected_service_index: any = -1;
  selected_gallery_index: any = -1;
  selected_location_index: any = -1;
  selected_testimonial_index: any = -1;
  selected_business_index: any = -1;

  curr_member_id: any;
  member_no: any;
  members: any;
  members1: any;
  business: any;
  states: any;
  cities: any;
  testemonialindex: any;
  servicedeleteindex: any;
  officeindex: any;
  galleryindex: any;
  user: any = null;
  busines: any = null;
  public latitude: number;
  public longitude: number;
  /*public searchControl: FormControl;*/
  public zoom: number;
  businesslocationForm: FormGroup;

  topologyFlag: boolean = false;
  constructor(private fb: FormBuilder, private router: Router, private mapsAPILoader: MapsAPILoader, private ngZone: NgZone, private _location: Location,
    public sharedService: SharedService, public sanitizer: DomSanitizer,public http:HttpClient) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.latitude = position.coords.latitude;
      this.longitude = position.coords.longitude;
      this.zoom = 12;
    });
  }
  ngAfterViewInit() {
    sessionStorage.removeItem('add-topography');
    setTimeout(() => {
      this.sharedService.SetHomeCategoryComponentFlag(false);
    }, 200);
  }
  allowurl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }


  ngOnInit(): void {
    this.loadTypologyData();
    const getData = this.sharedService.getCalculatorData();
    if (getData && getData.length > 0) {
      const getCalculatorData = JSON.parse(getData);
      if (getCalculatorData && getCalculatorData.Typology) {
        this.selectedTypology = getCalculatorData.Typology;
      }
    }
  }

  loadTypologyData() {
    this.sharedService.post("web_typology_list", '').subscribe((response: any) => {
      if (response.replyCode == 'success') {
        this.typologyData = response.data;
        this.topologyFlag = true;
      } else {
        this.topologyFlag = false;
      }
    }, err => {
      // this.sharedService.showError(err);
    });
  }

  saveTypology(typology) {
    var topologyData = this.typologyData.filter(x => x.id == typology);
    this.sharedService.setTopologyData(topologyData[0]);
    // const getData = this.sharedService.getCalculatorData();
    // if (getData && getData.length > 0) {
    //   const getCalculatorData = JSON.parse(getData);
    //   if (getCalculatorData) {
    //     getCalculatorData.Typology = typology;
    //     this.sharedService.setCalculatorData(getCalculatorData);
    //   }
    // }
  }
  fetchByUserID() {

    // this.setCurrentPosition(self.business.lat, self.business.lng);

    // const self = this;
    // /* this.httpClient.auth_token = this.user.jwt;*/
    // self.httpClient.loadingShow = true;
    // this.httpClient.get('businesses/fetchByUserID.json?id=' + this.user.id).map((res: Response) => res.json()).subscribe(function (res) {
    //   self.httpClient.loadingShow = false;
    //   if (!res.error) {
    //     self.business = res.data[0];
    //     self.setCurrentPosition(self.business.lat, self.business.lng);
    //     self.testemonialForm.patchValue({
    //       business_id: res.data[0].id
    //     })
    //   };

    //   self.officeForm.patchValue({
    //     business_id: res.data[0].id
    //   });

    //   self.galleryForm.patchValue({
    //     business_id: res.data[0].id
    //   });
    //   self.serviceForm.patchValue({
    //     business_id: res.data[0].id
    //   });

    //   self.zoom = 18;
    //   self.latitude = parseFloat(self.business.lat);
    //   self.longitude = parseFloat(self.business.lng);
    // });

  }
}
