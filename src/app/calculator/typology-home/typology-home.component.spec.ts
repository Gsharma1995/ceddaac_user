import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypologyHomeComponent } from './typology-home.component';

describe('CalculatorHomeComponent', () => {
  let component: TypologyHomeComponent;
  let fixture: ComponentFixture<TypologyHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypologyHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypologyHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
