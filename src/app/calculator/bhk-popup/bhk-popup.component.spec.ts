import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BHKPopupComponent } from './bhk-popup.component';

describe('Level2Component', () => {
  let component: BHKPopupComponent;
  let fixture: ComponentFixture<BHKPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BHKPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BHKPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
