import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-bhk-popup',
  templateUrl: './bhk-popup.component.html',
  styleUrls: ['./bhk-popup.component.css']
})
export class BHKPopupComponent implements OnInit {
  @Output() closePopUP = new EventEmitter<boolean>();
  @Output() selectedBhk = new EventEmitter();
  calculatorData: any;
  typeOfProjectLevel1Data: any;
  bhkDetails: any;
  currentLayout:any=0;

  constructor(public sharedService: SharedService,
    private router: Router, public sanitizer: DomSanitizer) { }
    ngAfterViewInit() {
      setTimeout(() => {
        this.sharedService.SetHomeCategoryComponentFlag(false);
      }, 200);
    }
  ngOnInit(): void {
    this.bhkDetails = JSON.parse(this.sharedService.getBhkDetails());
    this.bhkDetails.forEach(element => {
      var obj=element;
      Object.assign(obj,{class:''});
    });
    this.getCalculatorData();
  }
  allowurl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  getCalculatorData() {
    // const apiCalculatorData = this.sharedService.getApiCalculatorData();
    // const calculatorDataToSave = this.sharedService.getCalculatorData();
    // if (apiCalculatorData && apiCalculatorData.length > 0 && calculatorDataToSave && calculatorDataToSave.length > 0) {
    //   this.calculatorData = JSON.parse(calculatorDataToSave);
    //   const apiData = JSON.parse(apiCalculatorData);
    //   if (apiData && apiData.layouts && apiData.layouts.length > 0) {
    //     this.typeOfProjectLevel1Data = apiData.layouts;
    //     if (this.typeOfProjectLevel1Data && this.typeOfProjectLevel1Data.length > 0) {
    //       const data = this.typeOfProjectLevel1Data.filter(x => x.layoutId == this.calculatorData.TypeOfProjectLevel2ID);
    //       this.bhkDetails = data && data.length > 0 ? data[0] : null;
    //     }
    //   }
    // }
  }

  closeLevel2Model(closeModel) {
    this.closePopUP.emit(closeModel);
  }

  passNameToParent(data) {
    this.selectedBhk.emit(data);
  }


  onPrevious(){
    if(this.currentLayout>0)
    this.currentLayout--;
  }
  onNext(){
    if(this.currentLayout<this.bhkDetails.length-1)
    this.currentLayout++;
  }



}
