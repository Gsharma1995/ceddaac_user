import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-topography-home',
  templateUrl: './topography-home.component.html',
  styleUrls: ['./topography-home.component.css']
})
export class TopographyHomeComponent implements OnInit {
  topographyData: any;
  typeOfProject: any;
  openTopographyModel: boolean;
  selectedTopography: number;
  getCalculatorDatails:any;
  typologyDatails:any;
  typeofproject_class:any='bgBlur';
  topography_class:any='bgBlur';
  topography_flag:boolean=false;
  constructor(private router: Router,public sharedService: SharedService, public sanitizer: DomSanitizer) { }
  allowurl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.sharedService.SetHomeCategoryComponentFlag(false);
    }, 200);
  }
  ngOnInit(): void {
    this.getCalculatorData();
    const getData = this.sharedService.getCalculatorData();
    this.getCalculatorDatails = JSON.parse(this.sharedService.getTopologyData());
    const calculatorDataToSave = this.sharedService.getCalculatorData();
    this.calculatorData = JSON.parse(calculatorDataToSave)

    // if (getData && getData.length > 0) {
    //   // debugger
    //    this.getCalculatorDatails = JSON.parse(getData);
    //   if (this.getCalculatorDatails && this.getCalculatorDatails.TOPOGRAPHY) {
    //     this.selectedTopography = this.getCalculatorDatails.TOPOGRAPHY;
    //   }
    // }
    // const apiCalculatorData = this.sharedService.getApiCalculatorData() as any;
    // if (apiCalculatorData && apiCalculatorData.length > 0) {
    //   const apiData = JSON.parse(apiCalculatorData);
    //   if (apiData && apiData.data && apiData.data.length > 0) {
    //     // debugger
    //     var data = apiData.data.filter(x => x.inputId == this.getCalculatorDatails.Typology);
    //     if (data && data.length > 0) {
    //       this.typologyDatails = data[0];
    //     }
    //   }
    // }

  }

  getCalculatorData() {
    const getData = JSON.parse(this.sharedService.getTopologyData());
    const calculatorDataToSave = this.sharedService.getCalculatorData();
    this.calculatorData = JSON.parse(calculatorDataToSave)
    var request_data={
      'topology_id':getData.id
    }
    this.sharedService.post("web_typography_list", request_data).subscribe((response: any) => {
      if (response.replyCode == 'success') {
        this.topographyData = response.data;
        if(this.topographyData.length > 0){
          this.topography_class='';
          this.typeofproject_class='bgBlur';
          this.topography_flag=false;
        }else{
          this.topography_class='bgBlur';
          this.topography_flag=true;
          this.typeofproject_class='';
        }
      }
    }, err => {
    });

    var Object={
      "parent_id":"0",
      "city_id":this.calculatorData.city_id,
      'topology_id':getData.id
    }
    this.sharedService.post("web_type_of_project_list", Object).subscribe((response: any) => {
      if (response.replyCode == 'success') {
        this.typeOfProject = response.data;
      }
    }, err => {
    });

    // const getCalculatorData = this.sharedService.getApiCalculatorData() as any;
    // if (getCalculatorData && getCalculatorData.length > 0) {
    //   const data = JSON.parse(getCalculatorData);
    //   if (data && data.data && data.data.length > 0) {
    //     this.topographyData = data.data.filter(x => x.inputType == "TOPOGRAPHY");
    //     this.typeOfProject = data.data.filter(x => x.inputType == "TypeOfProject");
    //   }
    // }
  }
  saveTopographyDetails(topography) {
    sessionStorage.removeItem('add-topography');
    const getData = this.sharedService.getCalculatorData();
    this.sharedService.setTopographyData(topography);
    const getCalculatorData = JSON.parse(getData);
      getCalculatorData.TOPOGRAPHY = topography.typography_id;
      this.sharedService.setCalculatorData(getCalculatorData);
      this.openTopographyModel = true;
  }

  saveTopographyDetails123(topographyId, topographyName) {
    const getData = this.sharedService.getCalculatorData();
    if (getData && getData.length > 0) {
      const getCalculatorData = JSON.parse(getData);
      getCalculatorData.TOPOGRAPHY = topographyId;
      this.sharedService.setCalculatorData(getCalculatorData);
      this.openTopographyModel = true;
    }
  }

  closeTopographyModel(closeModel) {
    this.openTopographyModel = closeModel;
  }
  calculatorData: any;
  openCostPlanPopUp: boolean;
  selectedLevel1: number;
  apiData: any;

  // Flag
  SelectTypeProject:boolean=false;
  saveTypeofProjectDetails(typeOfProjects,typeOfProjectsName) {
    this.calculatorData.TypeOfProject = typeOfProjectsName;
    this.calculatorData.TypeOfProjectLevel1ID = typeOfProjects;
    this.sharedService.setCalculatorData(this.calculatorData);

    const getData = JSON.parse(this.sharedService.getTopologyData());

    var Object={
      "parent_id":this.calculatorData.TypeOfProjectLevel1ID,
      "city_id":this.calculatorData.city_id,
      'topology_id':getData.id
    }
    if(this.calculatorData.TypeOfProjectLevel1ID){
      this.sharedService.post("web_type_of_project_list", Object).subscribe((response: any) => {
        if (response.replyCode == 'success') {
          if(response.data.length > 0){
            this.router.navigate(['home/level2']);
          }else{
            this.router.navigate(['home/layouts']);
          }
        }
      }, err => {
      });
    }




    // this.router.navigate(['home/level2']);
      // this.router.navigate(['home/level1']);
    if(typeOfProjects == ''){
      this.SelectTypeProject = true;
    }else{
      this.SelectTypeProject = false; 
      // this.router.navigate(['home/level1']);
    }
  }
}
