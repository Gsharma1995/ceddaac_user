import { Component, ElementRef, OnInit, ViewChild, HostListener } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
import { DomSanitizer } from '@angular/platform-browser';
declare var $: any;
import ImageMap from "image-map";

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.css']
})
export class WorkspaceComponent implements OnInit {
  workSpace: any;
  calculatorData: any;
  bhkFilterData: any;
  openWorkspacePopup: boolean;
  subLayOutUrl: any;
  titleOfModel: string;
  sendToChild: any;
  subLayOutId: any;
  area: any;
  categoryItem: any;
  getCalculatorDatails: any;
  topographyData: any;
  workSpaceFlag: boolean = false;
  workSpaceData: any;
  SubLayoutData: any;

  AreaData: any = [
    { name: 'residetial' },
    { name: 'apartment' },
    { name: '2BHK' },
    { name: 'residetial' },
    { name: 'apartment' },
    { name: '2BHK' },
    { name: '2BHK' },
    { name: 'residetial' },
    { name: 'apartment' },
    { name: '2BHK' },
    { name: 'apartment' },
    { name: '2BHK' }
  ]
  ImageUrl: any;
  ImageTitle: any;
  @ViewChild('widgetsContent', { read: ElementRef }) public widgetsContent: ElementRef<any>;
  CategoryData: any;

  public Coordinate_Test = [{ name: 'test', coord: '421,528,383,596,424,672,416,828,455,850,486,836,548,726,613,610,581,583,544,568,461,508' }];
  screenHeight: any;
  screenWidth: any;
  constructor(public sharedService: SharedService, public sanitizer: DomSanitizer) { }
  public menuClick(id) {
    if ($('#menu-' + id).hasClass('active')) {
      $('#menu-' + id).removeClass('active');
    } else {
      $('.sidenav-item').removeClass('active');
      $('#menu-' + id).addClass('active');
    }
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.sharedService.SetHomeCategoryComponentFlag(false);
    }, 200);
  }
  selectPart(part: number) {
  }
  allowurl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    return event.target.innerWidth;
  }
  public scrollRight(): void {
    this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft + 150), behavior: 'smooth' });
  }

  public scrollLeft(): void {
    this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft - 150), behavior: 'smooth' });
  }

  ngOnInit(): void {
    // $('map').imageMapResize();
    $("body").on("click", ".image-map", function () {
      $("#myModal").modal("show");
      $("#myModal").find("#subimagetext").text(this.title);
      $(".blue").addClass("after_modal_appended");
      $('.modal-backdrop').appendTo('.blue');
      $('body').removeClass("modal-open")
      $('body').css("padding-right", "");
    });
    // ImageMap('img[usemap]', 500);
    // $('map').imageMapResize();
    // $('map').imageMapResize();
    $(function () {
      $(".progress-round").each(function () {
        var value = $(this).attr('data-value');
        var left = $(this).find('.progress-left .progress-bar');
        var right = $(this).find('.progress-right .progress-bar');
        if (value > 0) {
          if (value <= 50) {
            right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)')
          } else {
            right.css('transform', 'rotate(180deg)')
            left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)')
          }
        }
      })
      function percentageToDegrees(percentage) {
        return percentage / 100 * 360
      }
    });
    this.loadWorkSpace();
    const getData = this.sharedService.getCalculatorData();
    this.getCalculatorDatails = JSON.parse(this.sharedService.getTopologyData());
    this.topographyData = JSON.parse(this.sharedService.getTopographyData());
    const apiCalculatorData = this.sharedService.getCalculatorData() as any;
    this.calculatorData = JSON.parse(apiCalculatorData);
    this.sharedService.getCalculatorData();
  }

  loadWorkSpace() {
    this.bhkFilterData = JSON.parse(this.sharedService.getBhkDetails());
    this.sharedService.post("web_layout_list", '').subscribe((response: any) => {
      if (response.replyCode == 'success') {
        // this.workSpaceData = response.data;
        this.workSpaceData = response.data.filter(value => value.name == this.bhkFilterData.name);
        this.workSpaceFlag = true;
        this.sharedService.post("web_sub_layout_list", { 'layout_id': this.workSpaceData[0].id, 'type': '0' }).subscribe((response: any) => {
          if (response.replyCode == 'success') {
            if (response.data.length > 0) {
              this.SubLayoutData = response.data.filter(x => x.name != null);
              // this.allClass = response.data;
              $(document).ready(function () {
                // alert();
                $('map').imageMapResize();
                // $('img[usemap]').imageMapResize();
                ImageMap('img[usemap]', 500);
              });
            }
          }
        }, err => {
        });

      } else {
        this.workSpaceFlag = false;
      }
    }, err => {
    });

    this.sharedService.post("web_category_list", '').subscribe((response: any) => {
      if (response.replyCode == 'success') {
        // this.workSpaceData = response.data;
        this.CategoryData = response.data
      }
    }, err => {
    });


    // const apiCalculatorData = this.sharedService.getApiCalculatorData() as any;
    // const calculatorDataToSave = this.sharedService.getCalculatorData();
    // if (apiCalculatorData && apiCalculatorData.length > 0) {
    //   const apiData = JSON.parse(apiCalculatorData);

    //   if (calculatorDataToSave && calculatorDataToSave.length > 0 && apiData && apiData.data && apiData.data.length > 0 && apiData.layouts && apiData.layouts.length > 0) {
    //     this.calculatorData = JSON.parse(calculatorDataToSave);
    //     var bhkId = this.calculatorData.TypeOfProjectLevel2ID
    //     const data = apiData.layouts.filter(x => x.layoutId == bhkId);
    //     if (data && data.length > 0) {
    //       this.bhkFilterData = data[0];
    //     }
    //   }
    // }



    // this.sharedService.getData('Calculator/GetSubLayouts?layoutid=' + bhkId).subscribe((resp: any) => {
    //   if (resp && resp.errorNY == "N" && resp.data && resp.data.length > 0) {
    //     this.workSpace = resp.data;
    //   }

    // });
    this.workSpace = [
      {
        "subLayoutId": 1,
        "description": "Living/Dining",
        "cooordinates": "449,1,358,86,307,72,313,113,261,164,283,263,275,273,253,264,245,269,249,307,314,337,118,545,106,533,3,641,21,709,307,866,358,808,386,768,382,725,557,519,553,454,585,421,627,442,634,487,655,501,671,418,670,385,760,279,760,128,689,99",
        "title": "Living/Dining",
        "subLayoutUrl": "http://148.72.212.180/CeddaacAPI/resources/graphics/2bhk_LivingDining.png",
        "areaTypeId": 7,
        "length": 10.0,
        "height": 10.0,
        "width": 10.0,
        "scalingFactor": 1.0,
        "layoutId": 2,
        "isActive": true,
        "createdDate": "2020-09-09T21:38:01.7",
        "modifiedDate": null,
        "createdBy": 1,
        "modifiedBy": null,
        "layout": null
      },
      {
        "subLayoutId": 2,
        "description": "KITCHEN",
        "cooordinates": "386,769,383,725,557,519,553,456,585,420,626,440,633,491,800,569,605,834,606,848,585,877",
        "title": "KITCHEN",
        "subLayoutUrl": "http://148.72.212.180/CeddaacAPI/resources/graphics/2bhk_kitchen.png",
        "areaTypeId": 19,
        "length": 10.0,
        "height": 10.0,
        "width": 10.0,
        "scalingFactor": 1.0,
        "layoutId": 2,
        "isActive": true,
        "createdDate": "2020-09-09T21:40:13.45",
        "modifiedDate": null,
        "createdBy": 1,
        "modifiedBy": null,
        "layout": null
      },
      {
        "subLayoutId": 3,
        "description": "BEDROOM 1",
        "cooordinates": "615,866,610,830,801,570,885,624,1162,762,993,1026,965,1057",
        "title": "BEDROOM 1",
        "subLayoutUrl": "http://148.72.212.180/CeddaacAPI/resources/graphics/2bhk_Bedroom1.png",
        "areaTypeId": 1,
        "length": 10.0,
        "height": 10.0,
        "width": 10.0,
        "scalingFactor": 1.0,
        "layoutId": 2,
        "isActive": true,
        "createdDate": "2020-09-09T21:42:05.423",
        "modifiedDate": null,
        "createdBy": 1,
        "modifiedBy": null,
        "layout": null
      },
      {
        "subLayoutId": 4,
        "description": "BEDROOM 2",
        "cooordinates": "1118,81,1509,237,1461,411,1351,581,1080,458,1025,535,1020,577,905,521,908,479,988,370,1096,223",
        "title": "BEDROOM 2",
        "subLayoutUrl": "http://148.72.212.180/CeddaacAPI/resources/graphics/2bhk_Bedroom2.png",
        "areaTypeId": 1,
        "length": 10.0,
        "height": 10.0,
        "width": 10.0,
        "scalingFactor": 1.0,
        "layoutId": 2,
        "isActive": true,
        "createdDate": "2020-09-09T21:43:00.527",
        "modifiedDate": null,
        "createdBy": 1,
        "modifiedBy": null,
        "layout": null
      },
      {
        "subLayoutId": 5,
        "description": "TOILET 1",
        "cooordinates": "1164,759,1464,907,1367,1104,1044,946",
        "title": "TOILET 1",
        "subLayoutUrl": "http://148.72.212.180/CeddaacAPI/resources/graphics/2bhk_Bedroom1_Toilet1.png",
        "areaTypeId": 2,
        "length": 10.0,
        "height": 10.0,
        "width": 10.0,
        "scalingFactor": 1.0,
        "layoutId": 2,
        "isActive": true,
        "createdDate": "2020-09-09T21:44:37.76",
        "modifiedDate": null,
        "createdBy": 1,
        "modifiedBy": null,
        "layout": null
      },
      {
        "subLayoutId": 6,
        "description": "TOILET 2",
        "cooordinates": "986,29,864,174,862,318,990,374,1097,226,1118,81",
        "title": "TOILET 2",
        "subLayoutUrl": "http://148.72.212.180/CeddaacAPI/resources/graphics/2bhk_Bedroom1_Toilet2.png",
        "areaTypeId": 2,
        "length": 10.0,
        "height": 10.0,
        "width": 10.0,
        "scalingFactor": 1.0,
        "layoutId": 2,
        "isActive": true,
        "createdDate": "2020-09-09T21:45:02.637",
        "modifiedDate": null,
        "createdBy": 1,
        "modifiedBy": null,
        "layout": null
      },
      {
        "subLayoutId": 7,
        "description": "TOILET",
        "cooordinates": "762,128,862,169,860,320,986,373,909,484,904,527,672,417,670,384,762,277",
        "title": "TOILET",
        "subLayoutUrl": "http://148.72.212.180/CeddaacAPI/resources/graphics/2bhk_CommonToilet.png",
        "areaTypeId": 2,
        "length": 10.0,
        "height": 10.0,
        "width": 10.0,
        "scalingFactor": 1.0,
        "layoutId": 2,
        "isActive": true,
        "createdDate": "2020-09-09T21:45:50.09",
        "modifiedDate": null,
        "createdBy": 1,
        "modifiedBy": null,
        "layout": null
      },
      {
        "subLayoutId": 8,
        "description": "STUDY",
        "cooordinates": "1419,608,1080,462,1024,536,1021,578,980,591,937,651,1292,821",
        "title": "STUDY",
        "subLayoutUrl": "http://148.72.212.180/CeddaacAPI/resources/graphics/2bhk_Study.png",
        "areaTypeId": 6,
        "length": 10.0,
        "height": 10.0,
        "width": 10.0,
        "scalingFactor": 1.0,
        "layoutId": 2,
        "isActive": true,
        "createdDate": "2020-09-09T21:47:15.45",
        "modifiedDate": null,
        "createdBy": 1,
        "modifiedBy": null,
        "layout": null
      },
      {
        "subLayoutId": 9,
        "description": "BALCONY",
        "cooordinates": "1046,945,962,1061,941,1056,938,1088,937,1096,934,1125,1233,1285,1266,1288,1360,1120,1364,1100",
        "title": "BALCONY",
        "subLayoutUrl": "http://148.72.212.180/CeddaacAPI/resources/graphics/2bhk_Balcony.png",
        "areaTypeId": 5,
        "length": 10.0,
        "height": 10.0,
        "width": 10.0,
        "scalingFactor": 1.0,
        "layoutId": 2,
        "isActive": true,
        "createdDate": "2020-09-09T21:48:20.637",
        "modifiedDate": null,
        "createdBy": 1,
        "modifiedBy": null,
        "layout": null
      }
    ]
  }


  openCloseWorkspaceModel(openClose) {
    this.openWorkspacePopup = openClose;
    if (!openClose) {
      this.sharedService.getData('Calculator/GetAreaCategories?areaLayoutId=' + this.subLayOutId).subscribe((resp: any) => {
        if (resp && resp.errorNY == "N" && resp.data && resp.data.length > 0) {
          this.area = resp.data;
        } else {
          this.area = [];
        }
      });
    }
  }

  sendId(imageUrl, title, id) {
    this.subLayOutId = id;
    this.sendToChild =
    {
      "imageUrl": imageUrl,
      "title": title,
    }
  }

  getCategoryId(id) {
    this.sharedService.getData('Calculator/GetChildCategories?categoryID=' + id).subscribe((resp: any) => {
      if (resp && resp.errorNY == "N" && resp.data && resp.data.length > 0) {
        this.categoryItem = resp.data;
      } else {
        this.categoryItem = [];
      }
    });
  }
  // onClick(type){
  //   $('#myModalPopup').modal('show');
  //   alert(type)
  //   // if(type == 'bedroom'){
  //   //   alert(type)
  //   // }
  // }
  onClick(event) {
  }
  onModalPopup(event) {

    this.ImageUrl = event['url'];
    this.ImageTitle = event['name'];
    $('#myModalPopup').modal('show');
  }
  closeModel() {
    $('#myModalPopup').modal('hide');
  }
}
