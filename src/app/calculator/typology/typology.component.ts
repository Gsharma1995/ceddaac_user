import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/shared/shared.service';
import { DomSanitizer } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

// declare const google: any
import { MapsAPILoader, MouseEvent } from '@agm/core';

import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-typology',
  templateUrl: './typology.component.html',
  styleUrls: ['./typology.component.css']
})
export class TypologyComponent implements OnInit {
  typologyForm: FormGroup;
  SliderTemp = 0;
  typology_id = 0;
  calculatorDetails: any;
  submitted: boolean;
  areaUnit: string;
  topologyFlag: boolean = false;


  title = 'My first AGM project';
  public latitude: number;
  public longitude: number;
  address: string;
  private geoCoder;
  public zoom: number = 10;
  @ViewChild('search')
  public searchElementRef: ElementRef;
  loadLocation: any;
  keyword = 'name';
  // cities = [
  //   {
  //     name: 'Arkansas',
  //     population: '2.978M',
  //     flag: 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arkansas.svg'
  //   },
  //   {
  //     name: 'California',
  //     population: '39.14M',
  //     flag: 'https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg'
  //   },
  //   {
  //     name: 'Florida',
  //     population: '20.27M',
  //     flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Florida.svg'
  //   },
  //   {
  //     name: 'Texas',
  //     population: '27.47M',
  //     flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Texas.svg'
  //   }
  // ];
  cities:any=[];
  calculatorData: any;
  public data_flag:boolean=false;
  public data_value:any='';
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    public sharedService: SharedService,
    public sanitizer: DomSanitizer,public http:HttpClient) {
    this.calculatorDetails = JSON.parse(this.sharedService.getTopologyData());
  }
  public countries: any = [];
  selectEvent(item) {
    console.log('item::::::::::',item);
    // do something with selected item
    this.calculatorData.city_id = item.head_office;
    this.calculatorData.selected_city_id = item.id;
    this.sharedService.setCalculatorData(this.calculatorData);
    this.sharedService.SetCityName(item.name)
    if(item.id != item.head_office){
      this.data_flag=true;
      this.data_value=item.head_office_name;
    }else{
      this.data_flag=false;
    }
  }

  onChangeSearch(search: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e) {
    // do something
  }
  allowurl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.sharedService.SetHomeCategoryComponentFlag(false);
    }, 200);
  }
  ngOnInit(): void {

    const calculatorDataToSave = this.sharedService.getCalculatorData();
    this.calculatorData = JSON.parse(calculatorDataToSave)
    this.sharedService.getlocation().subscribe(data => {
      this.loadLocation = data.isp;
    })

    //load Places Autocomplete
   
    this.typologyForm = this.formBuilder.group({
      Location: ['', [Validators.required]],
      AreaType: ['', [Validators.required]],
      Area: ['', [Validators.required, Validators.min(0), Validators.max(10000), Validators.pattern("^[0-9]*$")]],
    });
    this.getCitiesList();
  }

  get f() { return this.typologyForm.controls; }

  getCitiesList() {
    this.sharedService.post('city_drop_down_list', {}).subscribe((data:any) => {
      this.cities=data.data;
    })
  }
  setSliderValue(value) {
    this.typologyForm.get('Area').setValue(value);
  }

  setSliderRange() {
    const value = this.typologyForm.get('Area').value;
    if (value && value.length > 0 && /^\d+$/.test(value)) {
      this.SliderTemp = value;
    } else {
      this.SliderTemp = 0;
    }
  }

  typologySubmit() {
    var location_coordinate = this.latitude + ',' + this.longitude;
    this.typologyForm.controls['Location'].setValue(location_coordinate);
    this.submitted = true;
    if (this.typologyForm.valid) {
      const getData = this.sharedService.getCalculatorData();
      if (getData && getData.length > 0) {
        const getCalculatorData = JSON.parse(getData);
        getCalculatorData.LocationText = this.typologyForm.get('Location').value;
        getCalculatorData.Area = this.typologyForm.get('Area').value;
        getCalculatorData.Unit = this.typologyForm.get('AreaType').value;
        if (getCalculatorData.Unit == 'Sq.m') {
          var Areainsqft = getCalculatorData.Area * 10.76;
        }
        else if (getCalculatorData.Unit == 'Sq.yd') {
          Areainsqft = getCalculatorData.Area * 9;
        }
        else if (getCalculatorData.Unit == 'acres') {
          Areainsqft = getCalculatorData.Area * 43560;
        }
        else if (getCalculatorData.Unit == 'Sq.ft') {
          Areainsqft = getCalculatorData.Area;
        }
        getCalculatorData.Areainsqft = Areainsqft;
        this.sharedService.setCalculatorData(getCalculatorData);
        this.router.navigate(['home/topography-home']);
      }
    }
  };

  getUnit(unit) {
    if (unit && unit.length > 0) {
      this.areaUnit = unit
      this.typologyForm.get("AreaType").setValue(unit);
    }
  }

  navigateToTypology() {
    this.router.navigate(['home/typology']);
  }
}
