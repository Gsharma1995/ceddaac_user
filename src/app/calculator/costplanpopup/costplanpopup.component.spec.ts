import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostplanpopupComponent } from './costplanpopup.component';

describe('CostplanpopupComponent', () => {
  let component: CostplanpopupComponent;
  let fixture: ComponentFixture<CostplanpopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostplanpopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostplanpopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
