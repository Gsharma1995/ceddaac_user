import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/shared/shared.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-costplanpopup',
  templateUrl: './costplanpopup.component.html',
  styleUrls: ['./costplanpopup.component.css']
})
export class CostplanpopupComponent implements OnInit {
  @Output() closepopUp = new EventEmitter();
  allClass: any;
  classDetails: any;
  calculatorDataToSave: any;
  planName: any;
  calculatorData: any;

  constructor(public sharedService: SharedService,
    private router: Router, public sanitizer: DomSanitizer) { }
  allowurl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.sharedService.SetHomeCategoryComponentFlag(false);
    }, 200);
  }
  ngOnInit(): void {
    this.getCalculatorData();
    this.calculatorDataToSave = this.sharedService.getCalculatorData();
    if (this.calculatorDataToSave && this.calculatorDataToSave.length > 0) {
      this.classDetails = JSON.parse(this.calculatorDataToSave);
    }
  }

  getCalculatorData() {
    // debugger
    const apiCalculatorData = this.sharedService.getCalculatorData() as any;

    this.calculatorData = JSON.parse(apiCalculatorData);
    // if (apiCalculatorData && apiCalculatorData.length > 0) {
    //   const apiData = JSON.parse(apiCalculatorData);
    //   if (apiData && apiData.classification && apiData.classification.length > 0) {
    //     this.allClass = apiData.classification;
    //   }
    // }
    const getData = JSON.parse(this.sharedService.getTopologyData());
    var Object={
      "city_id":this.calculatorData.city_id,
      'topology_id':getData.id
    }
    this.sharedService.post("web_finishing_list", Object).subscribe((response: any) => {
      if (response.replyCode == 'success') {
        if(response.data.length > 0){
          this.allClass = response.data;
          this.showPriceDetails(this.allClass[0]);
          this.hidePriceDetails(this.allClass[0]);
        }
      }
    }, err => {
    });
  }
  closeCostPlanPopUp(popUpClose) {
    this.closepopUp.emit(popUpClose);
  }

  saveClassDetails(classId) {
    if (this.classDetails) {
      this.classDetails.Class = classId;
      this.sharedService.setCalculatorData(this.classDetails);
      this.router.navigate(['home/workspace']);
    }
  }

  showPriceDetails(priceDetail) {
    // debugger
    this.planName = priceDetail;
  }

  hidePriceDetails(priceDetail) {
    this.planName = priceDetail;
  }
}
