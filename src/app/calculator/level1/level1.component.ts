import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-level1',
  templateUrl: './level1.component.html',
  styleUrls: ['./level1.component.css']
})
export class Level1Component implements OnInit {
  typeOfProject: any;
  calculatorData: any;
  topographyData: any;
  openCostPlanPopUp: boolean;
  selectedLevel1: number;
  typologyDatails: any;
  apiData: any;

  // Flag
  SelectTypeProject:boolean=false;
  constructor(public sharedService: SharedService,
    private actRoute: ActivatedRoute,
    private router: Router, public sanitizer: DomSanitizer) { }
    ngAfterViewInit() {
      setTimeout(() => {
        this.sharedService.SetHomeCategoryComponentFlag(false);
      }, 200);
    }
  ngOnInit(): void {
    this.sharedService.destoryBhkDetails();
    this.getCalculatorData();
  }
  allowurl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  getCalculatorData() {
    const apiCalculatorData = this.sharedService.getCalculatorData() as any;
    this.calculatorData = JSON.parse(apiCalculatorData);
    const getData = JSON.parse(this.sharedService.getTopologyData());
    var Object={
      "parent_id":"0",
      "city_id":this.calculatorData.city_id,
      'topology_id':getData.id
    }
    this.sharedService.post("web_type_of_project_list", Object).subscribe((response: any) => {
      if (response.replyCode == 'success') {
        this.typeOfProject = response.data;
      }
    }, err => {
    });
    if(sessionStorage.getItem("add-topography")){
      this.topographyData = JSON.parse(this.sharedService.getTopographyData());
    }
    
    this.typologyDatails = JSON.parse(this.sharedService.getTopologyData());
    // const apiCalculatorData = this.sharedService.getApiCalculatorData() as any;


    // if (apiCalculatorData && apiCalculatorData.length > 0 && calculatorDataToSave && calculatorDataToSave.length > 0) {
    //   this.apiData = JSON.parse(apiCalculatorData);
    //   this.calculatorData = JSON.parse(calculatorDataToSave)
    //   if (this.apiData && this.apiData.data && this.apiData.data.length > 0) {
    //     const data = this.apiData.data.filter(x => x.inputId == this.calculatorData.TOPOGRAPHY);
    //     const typologyData = this.apiData.data.filter(x => x.inputId == this.calculatorData.Typology);
    //     this.typeOfProject = this.apiData.data.filter(x => x.inputType == "TypeOfProject");

    //       this.selectedLevel1 = this.calculatorData.TypeOfProject;
          
    //     if (data && data.length > 0) {
    //       this.topographyData = data[0];
    //     }
    //     if (typologyData && typologyData.length > 0) {
    //       this.typologyDatails = typologyData[0];
    //     }
    //   }
    // }
  }

  navigateToTopography() {
    this.router.navigate(['home/topography-home']);
  }

  saveTypeofProjectDetails(typeOfProjects,typeOfProjectsName) {
    this.calculatorData.TypeOfProject = typeOfProjectsName;
    this.calculatorData.TypeOfProjectLevel1ID = typeOfProjects;
    this.calculatorData.TypeOfProjectLevel1Selected = typeOfProjectsName;
    this.calculatorData.TypeOfProjectID = typeOfProjects;
    this.calculatorData.TypeOfProjectLevel2ID = '';
    this.calculatorData.TypeOfProjectLevel2Selected = '';
    this.calculatorData.TypeOfProjectLevel3ID = '';
    this.calculatorData.TypeOfProjectLevel3Selected = '';
    this.sharedService.setCalculatorData(this.calculatorData);
    const getData = JSON.parse(this.sharedService.getTopologyData());
    // this.calculatorData = JSON.parse(apiCalculatorData);
    var Object={
      "parent_id":this.calculatorData.TypeOfProjectLevel1ID,
      "city_id":this.calculatorData.city_id,
      'topology_id':getData.id
    }
    if(this.calculatorData.TypeOfProjectLevel1ID){
      this.sharedService.post("web_type_of_project_list", Object).subscribe((response: any) => {
        if (response.replyCode == 'success') {
          if(response.data.length > 0){
            this.router.navigate(['home/level2']);
          }else{
            this.router.navigate(['home/level2']);
            // this.router.navigate(['home/layouts']);
          }
        }
      }, err => {
      });
    }
    if(typeOfProjects == ''){
      this.SelectTypeProject = true;
    }else{
      this.SelectTypeProject = false;
    }
    // if (this.apiData && this.apiData.data && this.apiData.data.length > 0) {
    //   const nextLevelData = this.apiData.data.filter(x => x.inputType == typeOfProjectsName);
    //   if (nextLevelData && nextLevelData.length > 0)
    //     this.router.navigate(['home/level2']);
    // }
  }

  openCloseCostPlanModel(openClosePopUp) {
    this.openCostPlanPopUp = openClosePopUp;
  }
}
