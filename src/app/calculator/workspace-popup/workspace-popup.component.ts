import { SharedService } from 'src/app/shared/shared.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-workspace-popup',
  templateUrl: './workspace-popup.component.html',
  styleUrls: ['./workspace-popup.component.css']
})
export class WorkspacePopupComponent implements OnInit {
  @Output() closePopup: EventEmitter<boolean> = new EventEmitter();
  @Input() myinputMsg:any;
  imageUrl:any;
  title:any;

  constructor(public sharedService:SharedService) { }
  ngAfterViewInit() {
    setTimeout(() => {
      this.sharedService.SetHomeCategoryComponentFlag(false);
    }, 200);
  }
  ngOnInit(): void {
     this.imageUrl = this.myinputMsg.imageUrl ;
     this.title = this.myinputMsg.title ;
  }

  closeModel(close) {
    this.closePopup.emit(close);
  }


}
