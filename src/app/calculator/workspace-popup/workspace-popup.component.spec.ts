import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkspacePopupComponent } from './workspace-popup.component';

describe('WorkspacePopupComponent', () => {
  let component: WorkspacePopupComponent;
  let fixture: ComponentFixture<WorkspacePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkspacePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkspacePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
