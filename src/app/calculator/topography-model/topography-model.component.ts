import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-topography-model',
  templateUrl: './topography-model.component.html',
  styleUrls: ['./topography-model.component.css']
})
export class TopographyModelComponent implements OnInit {
  topographyData: any;
  TopographyModelForm: FormGroup;
  submitted: boolean;
  calculatorData: any
  @Output() closeModel = new EventEmitter<boolean>();
  ValidationFlag: boolean = false;
  dataofsessionStorage = JSON.parse(this.sharedService.getCalculatorData());
  constructor(public sharedService: SharedService,
    private router: Router, private formBuilder: FormBuilder, public sanitizer: DomSanitizer) {
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.sharedService.SetHomeCategoryComponentFlag(false);
    }, 200);
  }
  allowurl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  ngOnInit(): void {
    this.TopographyModelForm = this.formBuilder.group({
      side_A: ['', [Validators.required]],
      side_B: ['', [Validators.required]],
      side_C: ['', [Validators.required]]
      // side_D: ['', [Validators.required]],
    });
    this.getApiCalculatorDatails();

    // const getData = this.sharedService.getCalculatorData();
    // if (getData && getData.length > 0) {
    //   const getCalculatorData = JSON.parse(getData);
    //   if (getCalculatorData && getCalculatorData.Length1) {
    //     this.TopographyModelForm.get('side_A').setValue(getCalculatorData.Length1);
    //   }
    //   if (getCalculatorData && getCalculatorData.Length2) {
    //     this.TopographyModelForm.get('side_B').setValue(getCalculatorData.Length2);
    //   }
    //   if (getCalculatorData && getCalculatorData.Length3) {
    //     this.TopographyModelForm.get('side_C').setValue(getCalculatorData.Length3);
    //   }
    //   if (getCalculatorData && getCalculatorData.Length4) {
    //     this.TopographyModelForm.get('side_D').setValue(getCalculatorData.Length4);
    //   }
    // }
  }

  get f() { return this.TopographyModelForm.controls; }


  getApiCalculatorDatails() {
    this.topographyData = JSON.parse(this.sharedService.getTopographyData());
    const calculatorDataToSave = this.sharedService.getCalculatorData();
    this.calculatorData = JSON.parse(calculatorDataToSave);
    console.log('this.topographyData::::',this.topographyData);
    if(this.topographyData.description == 'Square'){
      var side_a= Math.sqrt(this.dataofsessionStorage.Area).toFixed(2);
      this.TopographyModelForm.controls['side_A'].setValue(side_a);
    }
  }

  closeTopographyModel(closePopUp) {
    this.closeModel.emit(closePopUp);
  }

  saveTopographyModelDetailsBackup() {
    this.submitted = true;
    const data = this.sharedService.getCalculatorData();
    let dataToSave = data && data.length > 0 ? JSON.parse(data) : null;
    let isValid = false;
    if (this.TopographyModelForm.value.side_A > this.dataofsessionStorage.Area) {
      this.ValidationFlag = true;
    } else if ((this.TopographyModelForm.value.side_A * this.TopographyModelForm.value.side_B) > this.dataofsessionStorage.Area) {
      this.ValidationFlag = true;
    } else if ((this.TopographyModelForm.value.side_A * this.TopographyModelForm.value.side_B * this.TopographyModelForm.value.side_C) > this.dataofsessionStorage.Area) {
      this.ValidationFlag = true;
    } else if (this.TopographyModelForm.value.side_A != '' || this.TopographyModelForm.value.side_B != '' || this.TopographyModelForm.value.side_C != '') {
      this.ValidationFlag = false;
      dataToSave.Length1 = this.TopographyModelForm.get('side_A').value || '';
      dataToSave.Length2 = this.TopographyModelForm.get('side_B').value || '';
      dataToSave.Length3 = this.TopographyModelForm.get('side_C').value || '';
      this.router.navigate(['home/level1']);
    } else {
      return;
    }
  }

  // Calculation Part
  autofillvalue(value){
    console.log('value::::',value);
    var ObjectValue;
    if(value != ''){
      ObjectValue=value;
    }else{
      ObjectValue=0;
    }
    if(this.topographyData.description == 'Rectangle'){
      this.TopographyModelForm.value.side_A=ObjectValue;
      if(this.TopographyModelForm.value.side_A == 0){
        this.TopographyModelForm.controls['side_B'].setValue(0);
      }else{
        var side_b= this.dataofsessionStorage.Area/this.TopographyModelForm.value.side_A;
        let number_data:number=0;
        number_data= Math.floor(Number(side_b))
        console.log('number_data::::',number_data);
        this.TopographyModelForm.controls['side_B'].setValue(number_data);
      }
    }else if(this.topographyData.description == 'Parallelogram'){
      this.TopographyModelForm.value.side_A=ObjectValue;
      if(this.TopographyModelForm.value.side_A == 0){
        this.TopographyModelForm.controls['side_B'].setValue(0);
      }else{
        var side_b= this.dataofsessionStorage.Area/this.TopographyModelForm.value.side_A;
        let number_data:number=0;
        number_data= Math.floor(Number(side_b))
        this.TopographyModelForm.controls['side_B'].setValue(number_data);
      }
    }else if(this.topographyData.description == 'Trapezium'){
    }
    console.log('SIde:::A:::::',this.TopographyModelForm.value.side_A);
    console.log('Side:::B::',this.TopographyModelForm.value.side_B);
    console.log('Side:::C::',this.TopographyModelForm.value.side_C);
    console.log('value:::::',value);
    console.log('type:::::',this.topographyData.description);
    
  }
  autofillvalueTrapezium(value){
    console.log('value:::::',value);
    console.log('SIde:::A:::::',this.TopographyModelForm.value.side_A);
    console.log('Side:::B::',this.TopographyModelForm.value.side_B);
    console.log('Side:::C::',this.TopographyModelForm.value.side_C);
    if(this.TopographyModelForm.value.side_A && this.TopographyModelForm.value.side_C){
      // alert(1);
      var sideA= this.TopographyModelForm.value.side_A;
      var sideC= this.TopographyModelForm.value.side_C;
      var sideB=0;
      sideB= Math.floor((2*this.dataofsessionStorage.Area)/(this.TopographyModelForm.value.side_A*this.TopographyModelForm.value.side_C));
      this.TopographyModelForm.controls['side_B'].setValue(sideB);
      // var value: number = ((this.TopographyModelForm.value.side_A * this.TopographyModelForm.value.side_B) / 2) * this.TopographyModelForm.value.side_C;
    }else if(this.TopographyModelForm.value.side_A && this.TopographyModelForm.value.side_B){
      var sideC:any=0;
      sideC= Math.floor((2*this.dataofsessionStorage.Area)/(this.TopographyModelForm.value.side_A*this.TopographyModelForm.value.side_B));
      this.TopographyModelForm.controls['side_C'].setValue(sideC);
    }
  }



  getSquareArea() {
    if (this.TopographyModelForm.value.side_A != '') {
      var value: number = this.TopographyModelForm.value.side_A * this.TopographyModelForm.value.side_A;
      var Calculated_Area = (this.dataofsessionStorage.Area * 5) / 100;
      var min_Area: Number = Number(this.dataofsessionStorage.Area) - Calculated_Area;
      var max_Area: Number = Number(this.dataofsessionStorage.Area) + Calculated_Area;
      if (value >= min_Area && value <= max_Area) {
        this.ValidationFlag = false;
        const data = JSON.parse(this.sharedService.getCalculatorData());
        data.Length1 = this.TopographyModelForm.get('side_A').value;
        data.SelectedAreainsqft = value;
        this.sharedService.setCalculatorData(data);
        this.router.navigate(['home/level1']);
      } else {
        this.ValidationFlag = true;
      }
    } else {
      return;
    }
  }
  getRectangleArea() {
    if (this.TopographyModelForm.value.side_A != '' && this.TopographyModelForm.value.side_B != '') {
      var value: number = this.TopographyModelForm.value.side_A * this.TopographyModelForm.value.side_B;
      var Calculated_Area = (this.dataofsessionStorage.Area * 5) / 100;
      var min_Area: Number = Number(this.dataofsessionStorage.Area) - Calculated_Area;
      var max_Area: Number = Number(this.dataofsessionStorage.Area) + Calculated_Area;
      if (value >= min_Area && value <= max_Area) {
        this.ValidationFlag = false;
        const data = JSON.parse(this.sharedService.getCalculatorData());
        data.Length1 = this.TopographyModelForm.get('side_A').value;
        data.Length2 = this.TopographyModelForm.get('side_B').value;
        data.SelectedAreainsqft = value;
        this.sharedService.setCalculatorData(data);
        this.router.navigate(['home/level1']);
      } else {
        this.ValidationFlag = true;
      }
    } else {
      return;
    }
  }
  getTrapeziumArea() {
    if (this.TopographyModelForm.value.side_A != '' && this.TopographyModelForm.value.side_B != '' && this.TopographyModelForm.value.side_C != '') {
      var value: number = ((this.TopographyModelForm.value.side_A * this.TopographyModelForm.value.side_B) / 2) * this.TopographyModelForm.value.side_C;
      var Calculated_Area = (this.dataofsessionStorage.Area * 5) / 100;
      var min_Area: Number = Number(this.dataofsessionStorage.Area) - Calculated_Area;
      var max_Area: Number = Number(this.dataofsessionStorage.Area) + Calculated_Area;
      console.log('min_Area::::',min_Area);
      console.log('max_Area::::',max_Area);
      console.log('value::::',value);
      if (value >= min_Area && value <= max_Area) {
        this.ValidationFlag = false;
        const data = JSON.parse(this.sharedService.getCalculatorData());
        data.Length1 = this.TopographyModelForm.get('side_A').value;
        data.Length2 = this.TopographyModelForm.get('side_B').value;
        data.Length3 = this.TopographyModelForm.get('side_C').value;
        data.SelectedAreainsqft = value;
        this.sharedService.setCalculatorData(data);
        this.router.navigate(['home/level1']);
      } else {
        this.ValidationFlag = true;
      }

    } else {
      return;
    }
  }
  getParallelogramArea() {
    if (this.TopographyModelForm.value.side_A != '' && this.TopographyModelForm.value.side_B != '') {
      var value: number = this.TopographyModelForm.value.side_A * this.TopographyModelForm.value.side_B;
      var Calculated_Area = (this.dataofsessionStorage.Area * 5) / 100;
      var min_Area: Number = Number(this.dataofsessionStorage.Area) - Calculated_Area;
      var max_Area: Number = Number(this.dataofsessionStorage.Area) + Calculated_Area;
      if (value >= min_Area && value <= max_Area) {
        this.ValidationFlag = false;
        const data = JSON.parse(this.sharedService.getCalculatorData());
        data.Length1 = this.TopographyModelForm.get('side_A').value;
        data.Length2 = this.TopographyModelForm.get('side_B').value;
        data.SelectedAreainsqft = value;
        this.sharedService.setCalculatorData(data);
        this.router.navigate(['home/level1']);
      } else {
        this.ValidationFlag = true;
      }
    } else {
      return;
    }
  }
  saveTopographyModelDetails() {
    this.submitted = true;
    if (this.topographyData.description == 'Square') {
      this.getSquareArea();
    } else if (this.topographyData.description == 'Rectangle') {
      this.getRectangleArea();
    } else if (this.topographyData.description == 'Trapezium') {
      this.getTrapeziumArea();
    } else if (this.topographyData.description == 'Parallelogram') {
      this.getRectangleArea();
    }
  }
}
