import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopographyModelComponent } from './topography-model.component';

describe('TopographyModelComponent', () => {
  let component: TopographyModelComponent;
  let fixture: ComponentFixture<TopographyModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopographyModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopographyModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
