import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { HttpConfigInterceptor } from './shared/Http-interceptor/token-interceptor';
import { AuthGuarGuard } from './shared/Auth-Guard/auth-guar.guard';
import { SharedModule } from './shared/shared.module';
import { LocationStrategy, HashLocationStrategy,APP_BASE_HREF } from '@angular/common';
import { NgxSpinnerModule } from "ngx-spinner";
import { LoginComponent } from './shared/components/login/login.component';
import { SignUpComponent } from './shared/components/sign-up/sign-up.component';
import { HomeEditProfileComponent } from './shared/components/edit-profile/home-edit-profile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VendorSignUpComponent } from './shared/components/vendor-sign-up/vendor-sign-up.component';
import { VendorLoginComponent } from './shared/components/vendor-login/vendor-login.component';
import { VendorCategoryComponent } from './shared/components/vendor-category/vendor-category.component';
import { AddVendorPortfolioComponent,ViewVendorPortfolioComponent} from './shared/components/vendor-portfolio';

import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import {
  GoogleLoginProvider,
  FacebookLoginProvider
} from 'angularx-social-login';
import { ForgotPasswordComponent } from './shared/components/forgot-password/forgot-password.component';
import { SubscribeComponent } from './shared/components/subscribe/subscribe.component';
import { ChangePasswordComponent } from './shared/components/change-password/change-password.component';
import { ReachOutComponent } from './directory/reach-out/reach-out.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignUpComponent,
    HomeEditProfileComponent,
    VendorLoginComponent,
    VendorSignUpComponent,
    VendorCategoryComponent,
    AddVendorPortfolioComponent,
    ViewVendorPortfolioComponent,
    ForgotPasswordComponent,
    SubscribeComponent,
    ChangePasswordComponent,
    ReachOutComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,NgMultiSelectDropDownModule.forRoot(),
    // AgmCoreModule.forRoot({
    //   apiKey: "AIzaSyBM2Z2OVrOA5QFYbiYdFgVwLj_ka8gsXfc",
    //   libraries: ["places"]
    // }),
    SocialLoginModule
  ],
  providers: [AuthGuarGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true
    },{
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '285808180678-dm7i4vemvkod4b1rggiejp0dcs29c089'
            )
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('757325495177643')
          }
        ]
      } as SocialAuthServiceConfig,
    }
    // {
    //   provide: LocationStrategy, useClass: HashLocationStrategy
    // }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
