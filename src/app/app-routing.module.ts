import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuarGuard } from './shared/Auth-Guard/auth-guar.guard';
import { VendorSignUpComponent } from './shared/components/vendor-sign-up/vendor-sign-up.component';
import { VendorLoginComponent } from './shared/components/vendor-login/vendor-login.component';
import { VendorCategoryComponent } from './shared/components/vendor-category/vendor-category.component';
import { AddVendorPortfolioComponent,ViewVendorPortfolioComponent} from './shared/components/vendor-portfolio';
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {path:'vendor-signup',component:VendorSignUpComponent},
  {path:'vendor-login',component:VendorLoginComponent},
  {path:'vendor-category',component:VendorCategoryComponent},
  // {path:'add-vendor-portfolio',component:AddVendorPortfolioComponent},
  // {path:'vendor-category',component:AddVendorPortfolioComponent},
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m =>
      m.HomeModule)
  },
  {
    // path: 'directory', canActivate : [AuthGuarGuard],
    path: 'directory',
    loadChildren: () => import('./directory/directory.module').then(m =>
      m.DirectoryModule)
  },
  {
    path: "**", redirectTo: "home"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    scrollPositionRestoration: 'enabled', // Add options right here
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
