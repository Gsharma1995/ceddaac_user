import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
import { DomSanitizer } from '@angular/platform-browser';
import * as Mapboxgl from 'mapbox-gl';

@Component({
    selector: 'app-map-box',
    templateUrl: './map-box.component.html',
    styleUrls: ['./map-box.component.css'],
})
export class MapBoxComponent implements OnInit {
    lat: number = 26.91667;
    lng: number = 75.86667;
    public latitude: number; 
    public longitude: number;
    public zoom: number;
    mapa: Mapboxgl.map;
    calculatorData: any;
    Map_Flag:boolean=false;
    constructor(
        public sharedService: SharedService,
        public sanitizer: DomSanitizer,
        public http: HttpClient) {

    }

    createMarcador(lng: number, lat: number) {
        const marker = new Mapboxgl.Marker({
            draggable: true
        }).setLngLat([lng, lat]).addTo(this.mapa);
    }

    getLatLng(placename) {
        this.http.get(`https://api.mapbox.com/geocoding/v5/mapbox.places/`+placename+`.json?access_token=pk.eyJ1IjoiY2VkZGFhYy1tYXBzIiwiYSI6ImNra212cWl1OTF2dnEyb3BnZThnNHdmb2kifQ.XIwwsr1kcX7obAkxJS1CiA`).subscribe((data:any) => {
            this.lng=data.features[0].center[0];
            this.lat=data.features[0].center[1];
            this.LoadMap(this.lng,this.lat);
            this.createMarcador(this.lng,this.lat);
            setTimeout(() => {
                // this.LoadMap(this.lng,this.lat); 
            }, 1000);
        })
    }
    ngOnInit(): void {
        Mapboxgl.accessToken = environment.mapbox_key;
        this.mapa = new Mapboxgl.Map({
            container: 'mapa-mapbox',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [this.lng,this.lat],
            zoom: 13
        });
        this.createMarcador(this.lng,this.lat);
        this.sharedService.getlocation().subscribe(data =>{
            if(data.city){
                setTimeout(() => {
                    this.LoadMap(this.lng,this.lat); 
                }, 1000);
                this.getLatLng(data.city);
            }
        })
        this.getLatLng('Delhi');
        this.sharedService.GetCityName().subscribe(data =>{
            if(data){
                this.getLatLng(data);
            }
            
        })
        const apiCalculatorData = this.sharedService.getCalculatorData() as any;
        this.calculatorData = JSON.parse(apiCalculatorData);
        
    }
    LoadMap(lon,lat){
        this.Map_Flag=true;
        Mapboxgl.accessToken = environment.mapbox_key;
        this.mapa = new Mapboxgl.Map({
            container: 'mapa-mapbox',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [lon,lat],
            zoom: 13
        });
        this.createMarcador(lon,lat);
    }
}
