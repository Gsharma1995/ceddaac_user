import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
declare const $: any;

import { IDropdownSettings } from "ng-multiselect-dropdown";

interface ICity {
    item_id: number;
    item_text: string;
}
@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
    encapsulation:ViewEncapsulation.None
})
export class SearchComponent implements OnInit {
    Category_Type: any;
    @ViewChild('seachKey', { static: false }) seachKey: ElementRef;
    // cities: Array<ICity> = [];
    selectedItems: Array<ICity> = [];
    dropdownSettings: IDropdownSettings = {};
    searchkey:any;
    venderservices:any;
    review:any="";
    city:any=236; 
    cities:any=[];
    SearchKeyFlag:boolean=false;
    keyword = 'name';
    constructor(public sharedService: SharedService) { }
    selectEvent(item) {
        this.city = item.id;
        if(item.id){
            this.onItemDeSelect('');
        }
      }
      onRemove(){
        this.city = '';
        this.onItemDeSelect('');
      }
      onChangeSearch(search: string) {
      }
    
      onFocused(e) {
      }
    ngOnInit(): void {
        this.sharedService.SetPortFolio_PopUp(false);
        this.sharedService.GetSearch_Input().subscribe(data => {
            if(data == ''){
                this.SearchKeyFlag=false;
            }else{
                this.SearchKeyFlag=true;
            }
            setTimeout(() => {
                //this.seachKey.nativeElement.value = data;
                this.searchkey=data;
            }, 1000);
        })
        this.sharedService.GetActiveMenuLink().subscribe(data => {
            this.Category_Type = data;
        })

        this.cities = [
            { item_id: 1, item_text: "New Delhi" },
            { item_id: 2, item_text: "Mumbai" },
            { item_id: 3, item_text: "Bangalore" },
            { item_id: 4, item_text: "Pune" },
            { item_id: 5, item_text: "Chennai" },
            { item_id: 6, item_text: "Navsari" }
        ];
       
        this.GetCityList();
        this.GetVenderServicesList();
        this.selectedItems = [];
        this.dropdownSettings = {
            singleSelection: false,
            defaultOpen: false,
            idField: "item_id",
            textField: "item_text",
            selectAllText: "Select All",
            unSelectAllText: "UnSelect All",
            itemsShowLimit: 3
        };

    }
    GetVendorList(keyword, type) {
        this.sharedService.SetActiveMenuLink(type);
        var RequestData = {
            'keyword': keyword,
            'type': type,
            vendor_services_id:this.selectedItems[0].item_id || '',
            city_id:this.city || '',
            sort_review:this.review || ''
        }
        this.sharedService.post('web_vendor_category_list', RequestData).subscribe((response: any) => {
            this.sharedService.SetVendorCategoryData(response.data);
        });
    }
    onSearch() {
        this.sharedService.SetBanner_Tempalte(false);
        this.GetVendorList(this.seachKey.nativeElement.value, this.Category_Type);
    }
    GetCityList(){
        this.sharedService.post('city_drop_down_list', {}).subscribe((response: any) => {
            // this.cities=[
            //     {
            //       id: 1,
            //       name: 'Usa'
            //     },
            //     {
            //       id: 2,
            //       name: 'England'
            //     }
            //  ];
            this.cities=response.data;
        });
    }
    GetVenderServicesList(){
        this.sharedService.post('vendor_services_dropdown_list', {}).subscribe((response: any) => {
           var array=[];
            response.data.forEach(element => {
                array.push(
                    { item_id: element.id, item_text: element.title }
                )
            });
            this.venderservices=array
           
        });
        
    }
    // GetCityList(){
    //     this.sharedService.post('city_drop_down_list', {}).subscribe((response: any) => {
    //         this.sharedService.SetVendorCategoryData(response.data);
    //     });
    // }
    // GetCategoryList(){
    //     this.sharedService.post('web_vendor_category_list', {}).subscribe((response: any) => {
    //         this.sharedService.SetVendorCategoryData(response.data);
    //     });
    // }
    onItemSelect(item: any) {
    }
    onItemDeSelect(item: any) {
        var requestdata={
            vendor_services_id:'',
            city_id:this.city || '',
            sort_review:this.review || '',
            'keyword': this.searchkey || '',
            'type': this.Category_Type || ''
        }
        this.sharedService.post('web_vendor_category_list', requestdata).subscribe((response: any) => {
            this.sharedService.SetVendorCategoryData(response.data);
        });
    }

    onSelectAll(items: any) {
    }

    onDropDownClose() {
    }
    onCityChange(cityid){
    }
    onChange(){
        var service_id;
        var city;
        var review;
        if(this.selectedItems.length != 0){
            service_id=this.selectedItems[0].item_id;
        }else{
            service_id=""
        }


        if(this.city != '' && this.city != null){
            city=this.city;
        }else{
            city=""
        }
        
        
        if(this.review != '' && this.review != null){
            review=this.review;
        }else{
            review=""
        }


        var requestdata={
            vendor_services_id:service_id,
            city_id:city,
            sort_review:review,
            'keyword': this.searchkey || '',
            'type': this.Category_Type || ''
        }
        this.sharedService.post('web_vendor_category_list', requestdata).subscribe((response: any) => {
            this.sharedService.SetVendorCategoryData(response.data);
        });
    }
    onFilter() {
    }
}
