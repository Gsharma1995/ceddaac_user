import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
import { DomSanitizer } from '@angular/platform-browser';
import { element } from 'protractor';
import { OwlOptions } from 'ngx-owl-carousel-o';
@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  @Input() filteredDetails: any;
  @Output() valueChange = new EventEmitter<boolean>();
  activeVendor: Array<number> = new Array<number>();
  CategoryListData: any; VendorCategoryListData: any;
  CategoryListDataArray: any = []; VendorCategoryListDataArray: any = [];
  prenextFlag: boolean = false;
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: true
  }
  constructor(
    public SharedService: SharedService,
    public sanitizer: DomSanitizer) { }
  All_Category_data: any = [];
  Design_Category_data: any = [];
  Art_Category_data: any = [];
  Architecture_Category_data: any = [];
  Construction_Category_data: any = [];
  Get_Link: string = '';
  public IndexingLength: number = 0;
  No_Record_Flag: boolean = false;
  User_flag: boolean = false;
  allowurl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  userDetails: any;
  opacity: any = 0;
  VendorIndexingArray: any = [];
  No_Record_Value = 0;

  changeStyle($event) {
    this.userDetails = this.SharedService.getUserDetails();
    if (this.userDetails && this.userDetails.length > 0) {
      this.User_flag = true;
      this.SharedService.SetPortFolio_PopUp(false);
    }
    else {
      this.SharedService.isDirectoryLoginClicked = "directory";
      this.User_flag = false;
    }
  }
  @HostListener('window:scroll', ['$event']) // for window scroll events
  onScroll() {
    this.SharedService.getLoggedInStatusVendor().subscribe(data => {
      if (data == true) {
        // this.Vendor_flag=true;
      } else {
        // this.Vendor_flag=false;
        if (!(sessionStorage.getItem('UserDetails'))) {
          // alert('PopUp')
          // this.SharedService.isDirectoryLoginClicked = "";
          // this.SharedService.setLogin(true);
        } else {

        }
      }
    })
  }
  ngOnInit(): void {
    this.SharedService.GetActiveMenuLink().subscribe(data => {
      this.Get_Link = data;
    })
    this.GetVendorCategoryData();
  }

  GetVendorCategoryData() {
    this.No_Record_Value = 0;
    this.No_Record_Flag = false;
    var no_record_array:any=[];
    this.SharedService.GetVendorCategoryData().subscribe((response: any) => {
      this.CategoryListData = response;
      // console.log('CategoryListData::::',this.CategoryListData);

      this.prenextFlag = false;
      this.CategoryListData.forEach((element) => {
        if (element.vendors.length > 6) {
          this.prenextFlag = true;
        }
      });
      this.Design_Category_data = this.CategoryListData.filter(x => x.type == 'Design');
      this.Art_Category_data = this.CategoryListData.filter(x => x.type == 'Art');
      this.Architecture_Category_data = this.CategoryListData.filter(x => x.type == 'Architecture');
      this.Construction_Category_data = this.CategoryListData.filter(x => x.type == 'Construction');

      this.No_Record_Value = 0;
      no_record_array=[];
      this.CategoryListData.forEach((element, index) => {
        console.log('Index:::', index, 'element::::', element.vendors.length);
        no_record_array.push(element.vendors.length);
        if (element.vendors.length > 1) {
          this.No_Record_Value = 1;
        }
      });
      console.log('no_record_array:::::',no_record_array);
      var sum = no_record_array.reduce((acc, cur) => acc + cur, 0);
      console.log('Sum:::::',sum);
      setTimeout(() => {
        if(sum > 0){
          this.No_Record_Flag =false;
        }else{
          this.No_Record_Flag=true;
        }
      }, 1000);
      console.log('No_Record_Flag:::::', this.No_Record_Flag);


    });
  }
  counter(i: number) {
    return new Array(Math.ceil(i));
  }

  showVendor(value, vendorId, activeVendors, allvendor) {
    var obj = {
      "vendor_id": vendorId
    }
    this.SharedService.post('web_vendor_details', obj).subscribe((resps: any) => {
      if (resps.data[0].portfolio.length > 0) {
        this.VendorIndexingArray = [];
        if (allvendor.length > 0) {
          allvendor.forEach((element, index) => {
            this.SharedService.post('web_vendor_details', { "vendor_id": element.id }).subscribe((resps: any) => {
              if (resps.data[0].portfolio.length > 0) {
                this.VendorIndexingArray.push(element.id);
              }
            });
          });
        }
        //
        // setTimeout(() => {
        this.userDetails = this.SharedService.getUserDetails();
        if (this.userDetails && this.userDetails.length > 0) {
          this.SharedService.SetVendorIndexingData(this.VendorIndexingArray);
          this.User_flag = true;
          this.activeVendor.push(activeVendors);
          this.SharedService.setActiveVendorId(vendorId);
          this.SharedService.setActiveVendors(activeVendors);
          this.valueChange.emit(value);
          this.SharedService.SetPortFolio_PopUp(true);
          this.SharedService.SetBanner_Tempalte(true);
          this.SharedService.SetVendor_Banner_Tempalte(false);
        }
        else {
          this.SharedService.isDirectoryLoginClicked = "";
          this.SharedService.setLogin(true);
          this.User_flag = false;
        }
        // }, 100);
      } else {
        this.SharedService.showError("Portfolio is not available for the given vendor. We will update it soon.");
      }
    });
  }
  onPrevious() {
    this.IndexingLength = this.IndexingLength - 6;
  }
  onNext() {
    this.IndexingLength = this.IndexingLength + 6;
  }
  trackByFunc(index, item) {
    return 4;
  }
  // getVendorData(vendors) {
  //   // console.log('vendors::::',vendors);
  //   if(vendors){
  //     vendors.forEach(element => {
  //       console.log('element:::',element.vendors.length)
  //     });
  //   }
  //   return
  // }
}
