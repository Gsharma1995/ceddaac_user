import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';

import { DomSanitizer,SafeResourceUrl } from '@angular/platform-browser';
import { StarRatingComponent } from 'ng-starrating';
declare const $: any;
@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {
  portfolioDetails: any;
  vendorDetails: any;
  vendorDetails_rating: number=0;
  activeVendorId: number;
  activeVendors: any;
  scriptsLoaded = false;
  showVendor:boolean=true;
  showReachOutPopup:boolean=false;
  showReviewPopup:boolean=false;
  ReamingRating:any;
  @Output() valueChange = new EventEmitter<boolean>();
  VendorIndexingArray:any=[];
  UpDownArrow:boolean=false;
  UpArrow:boolean=true;
  DownArrow:boolean=true;
  page_flag1:boolean=false;page_flag1_data:any;
  page_flag2:boolean=false;page_flag2_data:any;
  page_flag3:boolean=false;page_flag3_data:any;
  page_flag4:boolean=false;page_flag4_data:any;
  page_flag5:boolean=false;page_flag5_data:any;
  page_flag6:boolean=false;page_flag6_data:any;
  page_flag7:boolean=false;page_flag7_data:any;
  page_flag8:boolean=false;page_flag8_data:any;
  Video_Link:SafeResourceUrl;

  
  projectRating = 0;
  projectRatingArr=[];
  animeArr=[];
  counter;
  isHalf = false;
  book_flag:boolean=false;
  constructor(public sharedService: SharedService, public sanitizer: DomSanitizer) { }
  onReachOut(){
    // $('#videolink').modal('show');
    this.sharedService.SetReachOutPopUp(true);
  }
  onReview(){
    $('#form').modal('show');
    this.sharedService.SetReviewPopUp(true);
  }
  ngOnInit(): void {

    this.sharedService.GetReviewPopUp().subscribe(data =>{
      setTimeout(() => {
        this.getPortfolioData(this.activeVendorId);
      }, 1000);
    })
    $('#videolink').modal('show');
    this.sharedService.GetVendorIndexingData().subscribe(data =>{
      this.VendorIndexingArray=data;
      if(data.length > 1){
        this.UpDownArrow=true;
      }else{
        this.UpDownArrow=false;
      }
    })
    this.sharedService.GetReachOutPopUp().subscribe(data =>{
      this.showReachOutPopup=data;
    })
    this.sharedService.GetReviewPopUp().subscribe(data =>{
      this.showReviewPopup=data;
    })
    this.sharedService.getActiveVendorId().subscribe(value => {
      this.activeVendorId = value;
      sessionStorage.setItem('vendorId',JSON.stringify(this.activeVendorId));
    });
    this.getPortfolioData(this.activeVendorId);
    this.sharedService.getActiveVendors().subscribe(value => {
      this.activeVendors = value;
    });
  }
  allowurl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  getPortfolioData(vendorId) {
    var obj={"vendor_id":vendorId}
    this.book_flag=false;
    this.portfolioDetails=[];
    this.activeVendorId = vendorId;
    this.vendorDetails='';
    this.sharedService.post('web_vendor_details', obj).subscribe((resps: any) => {
      if (resps && resps.replyCode == "success") {
        if (resps && resps.data[0].portfolio && resps.data[0].portfolio.length > 0) {
          this.portfolioDetails = resps && resps.data[0].portfolio ? resps.data[0].portfolio : [];
          if (this.portfolioDetails && this.portfolioDetails.length > 0) {
          this.loadPortfolioScripts();
          this.scriptsLoaded
            ? setTimeout(() => {
              // alert();
              //@ts-ignore
              // window.Page.bb.update();
              // window.Page.bb.first();
              //@ts-ignore
              // window.Page.bb.next();
            }, 100)
            : this.loadPortfolioScripts();
            
            this.scriptsLoaded = true;
            this.book_flag=true;
          }
          this.portfolioDetails.sort(function (a, b) {
            return a.page_no - b.page_no;
          });
          this.portfolioDetails.forEach(element => {
            if(element.page_no == "1"){
              this.page_flag1=true;
              this.page_flag1_data=element;
            }else if(element.page_no == "2"){
              this.page_flag2=true;
              this.page_flag2_data=element;
            }else if(element.page_no == "3"){
              this.page_flag3=true;
              this.page_flag3_data=element;
            }else if(element.page_no == "4"){
              this.page_flag4=true;
              this.page_flag4_data=element;
            }else if(element.page_no == "5"){
              this.page_flag5=true;
              this.page_flag5_data=element;
            }else if(element.page_no == "6"){
              this.page_flag6=true;
              this.page_flag6_data=element;
            }else if(element.page_no == "7"){
              this.page_flag7=true;
              this.page_flag7_data=element;
            }else if(element.page_no == "8"){
              this.page_flag8=true;
              this.page_flag8_data=element;
            }
          });
          this.vendorDetails = resps && resps.data[0] ? resps.data[0] : null;
          this.vendorDetails_rating= resps.data[0].rating;
          this.ReamingRating =5-this.vendorDetails.rating;
        }
      }
    });
  }

  loadPortfolioScripts() {
    var dynamicScripts = ["assets/js/modernizr.custom.79639.js", "assets/js/jquery.jscrollpane.min.js",
      "assets/js/jquery.bookblock.js", "assets/js/page.js"];
    for (var i = 0; i < dynamicScripts.length; i++) {
      let node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = true;
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }


  showPreviousPortfolioVendor() {
      const currentIndex = this.VendorIndexingArray.indexOf(this.activeVendorId);
      const previousIndex = (currentIndex - 1) % this.VendorIndexingArray.length;
      this.VendorIndexingArray[previousIndex];
      var firstId=this.VendorIndexingArray[0];
      this.DownArrow=true;
      if(firstId == this.VendorIndexingArray[previousIndex]){
        this.UpArrow=false;
      }
      this.getPortfolioData(this.VendorIndexingArray[previousIndex]);
  }

  showNextPortfolioVendor() {
      const currentIndex = this.VendorIndexingArray.indexOf(this.activeVendorId);
      const nextIndex = (currentIndex + 1) % this.VendorIndexingArray.length;
      this.VendorIndexingArray[nextIndex];
      this.UpArrow=true;
        var lastId=this.VendorIndexingArray[this.VendorIndexingArray.length-1];
        if(lastId == this.VendorIndexingArray[nextIndex]){
          this.DownArrow=false;
        }
      this.getPortfolioData(this.VendorIndexingArray[nextIndex]);
  }

  removeJSFile() {
    var allsuspects = document.getElementsByTagName('script')
    for (var i = allsuspects.length; i >= 0; i--) {
      if (allsuspects[i] && allsuspects[i].getAttribute('src') != null && allsuspects[i].getAttribute('src').indexOf('assets/js/modernizr.custom.79639.js') != -1)
        allsuspects[i].parentNode.removeChild(allsuspects[i]);
      if (allsuspects[i] && allsuspects[i].getAttribute('src') != null && allsuspects[i].getAttribute('src').indexOf('assets/js/jquery.jscrollpane.min.js') != -1)
        allsuspects[i].parentNode.removeChild(allsuspects[i]);
      if (allsuspects[i] && allsuspects[i].getAttribute('src') != null && allsuspects[i].getAttribute('src').indexOf('assets/js/jquery.bookblock.js') != -1)
        allsuspects[i].parentNode.removeChild(allsuspects[i]);
      if (allsuspects[i] && allsuspects[i].getAttribute('src') != null && allsuspects[i].getAttribute('src').indexOf('assets/js/page.js') != -1)
        allsuspects[i].parentNode.removeChild(allsuspects[i]);
    }
  }

  iconClass = {
    0: 'fa fa-star-o',
    0.5: 'fa fa-star-half-o',
    1: 'fa fa-star'
  }


  fillStars(reviewRating) {
    var stars =[];
    var rating = Math.floor(reviewRating);
    // this.isHalf = reviewRating %1 !== 0? true : false;
    // console.log('rating::::::',rating);
    if(rating == 1){
      stars = [0];
    }else if(rating == 2){
      stars = [0, 0];
    }else if(rating == 3){
      stars = [0, 0, 0];
    }else if(rating == 4){
      stars = [0, 0, 0, 0];
    }else if(rating == 5){
      stars = [0, 0, 0, 0, 0];
    }
    var remaining_star:any = Number(reviewRating-rating).toFixed(2);
    // console.log('remaining_star::::::',remaining_star);
    if(remaining_star == 0.00)
    {
      this.isHalf=false;
    }else if(remaining_star >= 0.5){
      this.isHalf=false;
      stars.push(0)
    }else if(remaining_star < 0.5){
      this.isHalf=true;
    }
    return stars;
  }
  onBack(){
    this.showVendor = false;
    this.valueChange.emit(false);
    this.sharedService.SetPortFolio_PopUp(true);
  }
  onShowVideo(video_link){
    if(video_link != '' || video_link != null){
      this.Video_Link=this.sanitizer.bypassSecurityTrustResourceUrl(video_link);
      $('#video').modal('show');
    }
  }
  onChangePage(){
    this.page_flag1=true;
    this.page_flag2=false;
    this.page_flag3=false;
    this.page_flag4=false;
    this.page_flag5=false;
    this.page_flag6=false;
    this.page_flag7=false;
    this.page_flag8=false;
  }

  Page(){
				
    var config = {
        $bookBlock : $( '#bb-bookblock' ),
        $navNext : $( '#bb-nav-next' ),
        $navPrev : $( '#bb-nav-prev' )
      },
      init = function() {
        config.$bookBlock.bookblock( {
          orientation : 'horizontal',
          speed : 700
        } );
        initEvents();
      },
      initEvents = function() {

        var $slides = config.$bookBlock.children();
        
        // add navigation events
        config.$navNext.on( 'click touchstart', function() {
          config.$bookBlock.bookblock( 'next' );
          return false;
        } );

        config.$navPrev.on( 'click touchstart', function() {
          config.$bookBlock.bookblock( 'prev' );
          return false;
        } );

        // add keyboard events
        $( document ).keydown( function(e) {
          var keyCode = e.keyCode || e.which,
            arrow = {
              left : 37,
              up : 38,
              right : 39,
              down : 40
            };

          switch (keyCode) {
            case arrow.up:
              config.$bookBlock.bookblock( 'prev' );
              e.preventDefault();
              break;
            case arrow.down:
              config.$bookBlock.bookblock( 'next' );
              e.preventDefault();
              break;
          }

        } );
      };

      return { init : init };

  }

  updateStars() {
    this.projectRatingArr=[];
    this.isHalf = this.projectRating %1 !== 0? true : false;
    for(let i=0; i<this.projectRating;i++){
      this.animeArr.push(i)
    }
    console.log('projectRatingArr::::',this.animeArr);
  }
 getArrayValues(index) {
  // this.animeArr=[];
  //   setInterval(() => {
  //     if(index == this.projectRatingArr.length)
  //       return;
  //       console.log('this.projectRatingArr[index]::::',this.projectRatingArr[index]);
  //     this.animeArr.push(this.projectRatingArr[index]);
  //     index++;
  //   }, 50);
  //   console.log('this.animeArr[index]::::',this.animeArr);
  }
}
