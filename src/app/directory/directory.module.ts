import { DemoNumber,JoinPipe } from './review-popup/pipe';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DirectoryRoutingModule } from './directory-routing.module';
import { DirectoryComponent } from './directory.component';
import { BannerComponent } from './banner/banner.component';
import { CategoriesComponent } from './categories/categories.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { ReviewPopupComponent } from './review-popup/review-popup.component';
import { InputStarsComponent } from './input-stars/input-stars.component';
import { NgxStarRatingModule } from 'ngx-star-rating';
import { SearchComponent } from './search/search.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { CarouselModule } from 'ngx-owl-carousel-o';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { RatingModule } from 'ng-starrating';
@NgModule({
  declarations: [DirectoryComponent, BannerComponent, CategoriesComponent, PortfolioComponent, ReviewPopupComponent,InputStarsComponent,DemoNumber,SearchComponent,JoinPipe],
  imports: [
    CommonModule,
    DirectoryRoutingModule,RatingModule ,
    FormsModule,
    ReactiveFormsModule,
    NgxStarRatingModule,
    NgMultiSelectDropDownModule,
    CarouselModule,AutocompleteLibModule
  ]
})
export class DirectoryModule { }
