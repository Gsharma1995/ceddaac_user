import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { SharedService } from 'src/app/shared/shared.service';
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, FormsModule, FormGroup, Validators } from '@angular/forms';
declare const $: any;

@Component({
  selector: 'app-review-popup',
  templateUrl: './review-popup.component.html',
  styleUrls: ['./review-popup.component.css']
})
export class ReviewPopupComponent implements OnInit {
   @Input() portfolioDetails:any;
   @Input() childMessage: string;
   form: FormGroup;
  restaurant = 'Chili\'s';
  Addform: FormGroup;
  rating: number=0;
  public submitted = false;public btnsubmitted = false;
  constructor(private fb: FormBuilder,public sharedService:SharedService, public sanitizer: DomSanitizer,public router:Router) { }
  vendorDetails:any;
  get f() { return this.Addform.controls; }
  User_Data=JSON.parse(sessionStorage.getItem('UserDetails'));
  ReviewList:any;
  allowurl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  ngOnInit(): void {
    this.getPortfolioData();
    this.GetReviewList();
    this.form = this.fb.group({
      cleanliness: this.fb.control(null),
      quality: this.fb.control(2)
    });
    this.Addform = this.fb.group({
      // user_id: [this.User_Data.id, Validators.required],
      review: ['', [Validators.required, Validators.minLength(5),Validators.maxLength(500)]],
      rating: ['', Validators.required],
      // vendor_id: [JSON.parse(sessionStorage.getItem('vendorId')), Validators.required],
    });
  }
  onSubmitReview(){
    this.submitted =true;
    if (this.Addform.invalid) {
      return;
    } else {
      this.btnsubmitted = true;
      var requestData = this.Addform.value;
          requestData.user_id =this.User_Data.id; 
          requestData.vendor_id =JSON.parse(sessionStorage.getItem('vendorId')); 
      this.sharedService.post('create_review', requestData).subscribe((resp: any) => {
        if(resp.replyCode == "success"){
          this.submitted=false;
          this.btnsubmitted=false;
          this.GetReviewList();
          $('#form').modal('hide');
          this.sharedService.SetReviewPopUp(false);
          this.sharedService.showSuccess(resp.replyMsg);
        }else{
          this.sharedService.showError(resp.replyMsg);
        }
        this.Addform.reset();
      });
    }
  }
  GetReviewList(){
    this.sharedService.post('web_vendor_reviews',{vendor_id:JSON.parse(sessionStorage.getItem('vendorId')),user_id:this.User_Data.id}).subscribe((value:any) => {
      if(value.data.length > 0){
        this.ReviewList = value.data;
        this.Addform.controls['review'].setValue(value.data[0].review);
        this.Addform.controls['rating'].setValue(value.data[0].rating);
        this.ReviewList.forEach(element => {
          var obj = element;
            Object.assign(obj,{review_pending:5-Number(element.rating)})
        });
      }else{
        
        this.Addform.controls['review'].setValue('');
        this.Addform.controls['rating'].setValue('');
      }
    });
  }
// 
  // example method to send the star component dynamic messaging
  public ratingMessage(ratingName: string) {
    let messages = {};
    messages['cleanliness'] = [ 
      `${this.restaurant} was gross!` ,  
      `${this.restaurant} was pretty bad.` ,  
      `${this.restaurant} was acceptable.` ,  
      `${this.restaurant} was pretty good.` ,  
      `${this.restaurant} was great!`
    ];
    messages['quality'] = [ 
      `${this.restaurant} was unacceptable!` ,  
      `I couldn't eat the food at ${this.restaurant}.` ,  
      `${this.restaurant} food was acceptable.` ,  
      `${this.restaurant} was yummy.` ,  
      `${this.restaurant} is great!`
    ];
    return messages[ratingName];
  }
  getPortfolioData() {
    var obj={
      "vendor_id":JSON.parse(sessionStorage.getItem('vendorId'))
    }
    this.sharedService.post('web_vendor_details', obj).subscribe((resps: any) => {
      this.vendorDetails=resps.data[0].reviews;
      if (resps && resps.replyCode == "success") {
        
      }
    });
  }
}
