import { Component, OnInit, Input, Output, EventEmitter, ViewChild,ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../shared/shared.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { SocialAuthService } from "angularx-social-login";
@Component({
  selector: 'app-directory',
  templateUrl: './directory.component.html',
  styleUrls: ['./directory.component.css']
})
export class DirectoryComponent implements OnInit {
  subCategories: any;
  vendorDetails: any;
  filteredDetails: any;
  userName: string;
  currentCategoryId = 1;
  openSignup: boolean;
  showVendor: boolean = false;
  CategoryListData: any;
  VendorCategoryListData: any;
  VendorParentCategoryListData: any = [];
  index: number = 0;
  DirectoryCategoryMenu: any;
  @Output() valueChange = new EventEmitter<boolean>();
  Banner_Flag:boolean=false;
  @ViewChild('seachKey', { static: false }) seachKey: ElementRef;
  Category_Type:any='';
  Vendor_PortFolio_Flag:boolean=true;
  isLoggedIn: boolean;
  constructor(private router: Router, private sharedService: SharedService,private authService: SocialAuthService) {
    this.GetVendorList('',this.Category_Type);
    this.sharedService.SetSearch_Input('');
  //  this.sharedService.Banner_Tempalte = new BehaviorSubject<boolean>(false);
  }
  GetVendorList(keyword,type) {
    this.sharedService.SetActiveMenuLink(type);
    var RequestData = {
      'keyword': keyword,
      'type': type
    }
    this.sharedService.post('web_vendor_category_list', RequestData).subscribe((response: any) => {
      this.VendorParentCategoryListData = response.data;
      this.sharedService.SetVendorCategoryData(this.VendorParentCategoryListData);
    });
  }
  onClickLogin() {
    this.sharedService.isDirectoryLoginClicked = "";
    this.sharedService.setLogin(true);
  }

  onClickDirectory(value) {
    this.sharedService.isDirectoryLoginClicked = "directory";
    this.sharedService.setLogin(true);
  }

  openCloseSignuppopup() {
    this.sharedService.setSignup(true);
  }

  ngOnInit(): void {
    this.sharedService.getLoggedInStatus().subscribe(value => {
      let userDetails = this.sharedService.getUserDetails();
      if (userDetails && userDetails.length > 0) {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = value;
      }
    });
    this.sharedService.SetBanner_Tempalte(true);
    this.sharedService.GetSearch_Input().subscribe(data => {
      setTimeout(() => {
          //this.seachKey.nativeElement.value = data;
          // this.searchkey=data;
          if(data == ''){
            this.sharedService.SetBanner_Tempalte(true)
            // this.sharedService.Banner_Tempalte = new BehaviorSubject<boolean>(true);
          }else{
            this.seachKey.nativeElement.value = data;
            this.sharedService.SetBanner_Tempalte(false);
          }
      }, 1000);
  })
  this.sharedService.GetVendor_Banner_Tempalte().subscribe(data => {
    this.Vendor_PortFolio_Flag = data;
  })
    this.sharedService.GetPortFolio_PopUp().subscribe(data => {
      this.showVendor = data;
    })
    this.sharedService.GetBanner_Tempalte().subscribe(data => {
      this.Banner_Flag = data;
    })
    this.DirectoryCategoryMenu = [
      { name: 'All', link: '', index: 0, flag: false },
      { name: 'Design', link: 'Design', index: 1, flag: false },
      { name: 'Art', link: 'Art', index: 2, flag: false },
      { name: 'Architecture', link: 'Architecture', index: 3, flag: false },
      { name: 'Construction', link: 'Construction', index: 4, flag: false },
    ]
    this.DirectoryCategoryMenu.forEach(element => {
      if (element.index == this.index) {
        element.flag = true;
      }
    });
    this.loadSubcategory();
    this.sharedService.post('web_category_list', '').subscribe((response: any) => {
      this.CategoryListData = response.data;
    });
    this.sharedService.post('web_vendor_list', '').subscribe((response: any) => {
      this.VendorCategoryListData = response.data;
    });
  }

  loadHome() {
    this.sharedService.SetPortFolio_PopUp(false);
    this.sharedService.SetBanner_Tempalte(false);
    this.sharedService.SetVendor_Banner_Tempalte(true);
    this.router.navigate(['./home'])
  }

  loadSubcategory() {
    // this.sharedService.getData('DirectoryCategory').subscribe((resp: any) => {
    //   if (resp && resp.errorNY == "N" && resp.data && resp.data.length > 0) {
    //     this.subCategories = resp.data;
    //   }
    //   this.sharedService.getData('Vendor/GetDAACVendors?daaccategoryid=0').subscribe((response: any) => {
    //     if (response && response.errorNY == "N" && response.data && response.data.length > 0) {
    //       this.vendorDetails = response.data;
    //     }
    //     this.getCategorydata(1);
    //   });
    // });
  }

  getCategorydata(type, CategoryObject) {
    this.Category_Type=type;
    this.DirectoryCategoryMenu.forEach(element => {
      element.flag = false;
    });
    CategoryObject.flag = true;
    this.GetVendorList('',this.Category_Type);
    this.valueChange.emit(false);
    this.sharedService.SetPortFolio_PopUp(false);
  }

  getCategorydata_backup(categoryId) {
    this.showVendor = true;
    this.currentCategoryId = categoryId;
    // debugger
    if (this.subCategories && this.subCategories.length > 0) {
      this.filteredDetails = this.subCategories.filter(x => x.parentId == categoryId).
        map(subCategory => {
          return {
            daaccategoryId: subCategory.daaccategoryId,
            description: subCategory.description,
            parentId: subCategory.parentId,
            viewType: subCategory.viewType,
            vendors: this.vendorDetails.filter(x => x.daacCategoryId == subCategory.daaccategoryId)
          }
        });
    }
  }
  getUserDetail() {
    let currentUser = this.sharedService.getUserDetails()
    if (currentUser && currentUser.length > 0) {
      var user = JSON.parse(currentUser);
      this.userName = user.first_name + ' ' + user.last_name;
    }
  }
  signOut() {
    this.sharedService.signOut();
    this.sharedService.SetBanner_Tempalte(false);
    this.sharedService.SetSearch_Input('');
    this.sharedService.SetPortFolio_PopUp(false);
    this.sharedService.setLoggedInStatusVendor(false);
    sessionStorage.removeItem('user_type');
    this.authService.signOut();
  }
  openEditProfile() {
    this.sharedService.setEditProfile(true);
  }
  onChangePassword(){
    this.sharedService.SetChangePasswordOpen(true);
  }
  openVendorPopup(value) {
    this.showVendor = value;
    this.sharedService.SetPortFolio_PopUp(true);
  }
  onSearch(){
    if(this.seachKey.nativeElement.value)
    this.sharedService.SetBanner_Tempalte(false);
    this.sharedService.SetSearch_Input(this.seachKey.nativeElement.value);
    this.GetVendorList(this.seachKey.nativeElement.value,this.Category_Type);
  }
  onRedirectHomePage(){
    this.router.navigate(['./home'])
    this.sharedService.SetRouteType('works')
  }
}
