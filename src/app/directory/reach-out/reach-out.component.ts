import { DomSanitizer } from '@angular/platform-browser';
import { SharedService } from 'src/app/shared/shared.service';
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, FormsModule, FormGroup, Validators } from '@angular/forms';
declare const $: any;
@Component({
  selector: 'app-reach-out',
  templateUrl: './reach-out.component.html',
  styleUrls: ['./reach-out.component.css']
})
export class ReachOutComponent implements OnInit {
   @Input() portfolioDetails:any;
   @Input() childMessage: string;
   form: FormGroup;
  restaurant = 'Chili\'s';
  Addform: FormGroup;
  rating: number=0;
  public submitted = false;public btnsubmitted = false;
  constructor(private fb: FormBuilder,public sharedService:SharedService, public sanitizer: DomSanitizer) { }

  User_Data=JSON.parse(sessionStorage.getItem('UserDetails'));
  ReviewList:any;
  allowurl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  onClose(){
    this.sharedService.SetReachOutPopUp(false);
    $('#reach').modal('hide');
  }
  ngOnInit(): void {
    this.Addform = this.fb.group({
      title: ['', Validators.required],
      description: ['', Validators.required]
    });
  }
  get f() { return this.Addform.controls; }
  onSubmitReview(){
    this.submitted =true;
    if (this.Addform.invalid) {
      return;
    } else {
      this.btnsubmitted = true;
      var requestData = this.Addform.value;
          requestData.user_id =this.User_Data.id; 
          requestData.vendor_id =JSON.parse(sessionStorage.getItem('vendorId')); 
          requestData.type ='1'; 
      this.sharedService.post('send_feedback', requestData).subscribe((resp: any) => {
        if(resp.replyCode == "success"){
          this.submitted=false;
          this.btnsubmitted=false;
          this.sharedService.showSuccess('Query submitted successfully');
        }else{
          this.sharedService.showError(resp.replyMsg);
        }
        this.Addform.reset();
        $('#reach').modal('hide');
        this.sharedService.SetReachOutPopUp(false);
        this.submitted=false;
        this.btnsubmitted=false;
        // this.sharedService.showError("Oops! Something went wrong!");
      });
    }
  }
}
